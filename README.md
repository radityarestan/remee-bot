## REMEE BOT
[![pipeline status](https://gitlab.com/radityarestan/remee-bot/badges/master/pipeline.svg)](https://gitlab.com/radityarestan/remee-bot/-/commits/master)
[![coverage report](https://gitlab.com/radityarestan/remee-bot/badges/master/coverage.svg)](https://gitlab.com/radityarestan/remee-bot/-/commits/master)

### Kelompok C6
- 1906292982	Christian Raditya Restanto
- 1906293045	Gilang Catur Yudishtira
- 1906350723	Darren Ngoh
- 1906350805	Muhammad Haikal Ubaidillah Susanto
- 1906351045	Ilma Alpha Mannix

### Deskripsi
Remee adalah Discord Game Bot yang terdiri dari kumpulan Card Game.  Untuk bentuk permainannya akan dilakukan secara text based dan reaction. Permainan yang terdapat di Remee diantaranya adalah Kartu Setan dan Kartu 41.
#### Query yang terdapat dalam Remee :
- remee help : Menampilkan penjelasan mengenai bot Reeme dan query-query yang ada di Remee.
- remee play : Menginisiasi permainan dan mempersiapkan permainan.
- remee start : Memulai permainan.
- remee leave : Pemain keluar dari permainan yang sedang dimainkan.
- remee kick username_player : Mengeluarkan player dari game.
- remee stop : Menghentikan permainan yang sedang berlangsung.
- remee bye : Mengeluarkan remee dari server.

### [Let's invite remee to your server](https://discord.com/api/oauth2/authorize?client_id=835729628365979688&permissions=8&scope=bot)

### Overview
![1](https://cdn.discordapp.com/attachments/849318661054791723/850277862908362752/play.gif)
![2](https://cdn.discordapp.com/attachments/849318661054791723/850277860857479168/choose-game.gif)
![3](https://cdn.discordapp.com/attachments/849318661054791723/850277859515564033/join.gif)
![4](https://cdn.discordapp.com/attachments/849318661054791723/850277864422375424/start.gif)
![5](https://cdn.discordapp.com/attachments/849318661054791723/850277864384626688/ingame1.gif)
![6](https://cdn.discordapp.com/attachments/849318661054791723/850277853617979462/ingame2.gif)
![7](https://cdn.discordapp.com/attachments/849318661054791723/850308397411860490/ingame3.gif)
![8](https://cdn.discordapp.com/attachments/849318661054791723/850308368257384468/leaderboard.gif)

