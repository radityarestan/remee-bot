package id.ac.ui.cs.remeebot.core.listener;

import id.ac.ui.cs.remeebot.core.game.card.Card;
import id.ac.ui.cs.remeebot.core.game.gameplay.Game;
import id.ac.ui.cs.remeebot.core.game.gameplay.Kartu41Game;
import id.ac.ui.cs.remeebot.core.game.gameplay.KartuSetanGame;
import id.ac.ui.cs.remeebot.core.game.player.Kartu41Player;
import id.ac.ui.cs.remeebot.core.game.player.KartuSetanPlayer;
import id.ac.ui.cs.remeebot.core.game.player.Player;
import id.ac.ui.cs.remeebot.repository.CardRepository;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.requests.restaction.AuditableRestAction;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;
import java.util.Map;
import java.util.Stack;

import static id.ac.ui.cs.remeebot.core.game.ConstantEmote.ICON_CHECK;
import static id.ac.ui.cs.remeebot.core.game.ConstantEmote.ICON_X;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

public class QueryKickTest {
    private QueryKick queryKick;
    private Game testgame1;
    private Game testgame2;
    private Game testgame3;

    @Mock
    private GuildMessageReceivedEvent event;

    @Mock
    private MessageReactionAddEvent eventReaction;

    @Mock
    private Message message;

    @Mock
    private Member member;

    @Mock
    private User user1;

    @Mock
    private User user2;

    @Mock
    private User user3;

    @Mock
    private User user4;

    @Mock
    private User user5;

    @Mock
    private Player player1;

    @Mock
    private Kartu41Player player2;

    @Mock
    private Kartu41Player player6;

    @Mock
    private KartuSetanPlayer player3;

    @Mock
    private KartuSetanPlayer player4;

    @Mock
    private  KartuSetanPlayer player5;

    @Mock
    private Guild guild;

    @Mock
    private Map<Guild, Game> listGame = Game.listGame;

    @Mock
    private TextChannel channel1;

    @Mock
    private MessageAction messageAction;

    @Mock
    private RestAction<PrivateChannel> privateChannelRestAction;

    @Mock
    private CardRepository cardRepository;

    @Mock
    private Stack<Card> deck;

    @Mock
    AuditableRestAction deleteMessage;

    @Mock
    private List<Card> cardList;

    @Mock
    private Card dummyCard;

    @Mock
    MessageReaction.ReactionEmote reactionEmote;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        createGameAndGetPlayer();
        queryKick = new QueryKick();
    }

    public void createGameAndGetPlayer() {
        given(eventReaction.getTextChannel()).willReturn(channel1);
        testgame1 = new KartuSetanGame(eventReaction);
        testgame2 = new Kartu41Game(eventReaction);
        testgame3 = new KartuSetanGame(eventReaction);
    }

    @Test
    public void testKickInKartuSetanGame() {
        Game.listGame.put(guild, testgame1);
        testgame1.setCurrentMessageId("123");
        given(event.getAuthor()).willReturn(user3);
        given(event.getMember()).willReturn(member);
        given(member.getUser()).willReturn(user3);
        given(user3.isBot()).willReturn(false);
        given(event.getGuild()).willReturn(guild);
        given(event.getMessage()).willReturn(message);
        given(player3.getUser()).willReturn(user3);
        given(player4.getUser()).willReturn(user4);
        given(player5.getUser()).willReturn(user5);
        given(event.getChannel()).willReturn(channel1);
        given(event.getAuthor()).willReturn(user3);
        given(channel1.sendMessage(anyString())).willReturn(messageAction);
        given(user3.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user4.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user5.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(player3.getNextPlayer()).willReturn(player4);
        given(player4.getNextPlayer()).willReturn(player5);
        given(player5.getNextPlayer()).willReturn(player3);
        given(player3.getName()).willReturn("Player 3");
        given(player4.getName()).willReturn("Player4");
        given(player5.getName()).willReturn("Player 5");
        given(user3.getName()).willReturn("Player 3");
        given(user4.getName()).willReturn("Player4");
        given(user5.getName()).willReturn("Player 5");
        given(message.getContentRaw()).willReturn("remee kick Player4");
        given(channel1.deleteMessageById(anyString())).willReturn(deleteMessage);
        testgame1.joinGame(player3);
        testgame1.joinGame(player4);
        testgame1.joinGame(player5);
        testgame1.setPlayerWhoGetsTurn(player3);
        testgame1.startGame();
        queryKick.onGuildMessageReceived(event);
    }

    @Test
    public void testKickVoteYesKartuSetanGame() {
        Game.listGame.put(guild, testgame1);
        testgame1.setCurrentMessageId("123");
        testgame1.setPlayerWhoGetVoted(player5);
        given(eventReaction.getMember()).willReturn(member);
        given(member.getUser()).willReturn(user3);
        given(user3.isBot()).willReturn(false);
        given(eventReaction.getGuild()).willReturn(guild);
        given(player3.getUser()).willReturn(user3);
        given(player4.getUser()).willReturn(user4);
        given(player5.getUser()).willReturn(user5);
        given(eventReaction.getChannel()).willReturn(channel1);
        given(channel1.sendMessage(anyString())).willReturn(messageAction);
        given(user3.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user4.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user5.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(player3.getNextPlayer()).willReturn(player4);
        given(player4.getNextPlayer()).willReturn(player5);
        given(player5.getNextPlayer()).willReturn(player3);
        given(player3.getName()).willReturn("Player 3");
        given(player4.getName()).willReturn("Player 4");
        given(player5.getName()).willReturn("Player 5");
        given(user3.getName()).willReturn("Player 3");
        given(user4.getName()).willReturn("Player 4");
        given(user5.getName()).willReturn("Player 5");
        given(message.getContentRaw()).willReturn("remee kick Player 4");
        given(eventReaction.getReactionEmote()).willReturn(reactionEmote);
        given(eventReaction.getMessageId()).willReturn("1222");
        given(reactionEmote.getAsCodepoints()).willReturn(ICON_CHECK);
        given(channel1.deleteMessageById(anyString())).willReturn(deleteMessage);
        testgame1.getVoteList().add(player4);
        testgame1.joinGame(player3);
        testgame1.joinGame(player4);
        testgame1.joinGame(player5);
        testgame1.setPlayerWhoGetsTurn(player3);
        testgame1.startGame();
        queryKick.onMessageReactionAdd(eventReaction);
    }

    @Test
    public void testKickVoteKartuSetanGame() {
        Game.listGame.put(guild, testgame1);
        testgame1.setCurrentMessageId("123");
        testgame1.setPlayerWhoGetVoted(player5);
        given(eventReaction.getMember()).willReturn(member);
        given(member.getUser()).willReturn(user3);
        given(user3.isBot()).willReturn(false);
        given(eventReaction.getGuild()).willReturn(guild);
        given(player3.getUser()).willReturn(user3);
        given(player4.getUser()).willReturn(user4);
        given(player5.getUser()).willReturn(user5);
        given(eventReaction.getChannel()).willReturn(channel1);
        given(channel1.sendMessage(anyString())).willReturn(messageAction);
        given(user3.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user4.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user5.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(player3.getNextPlayer()).willReturn(player4);
        given(player4.getNextPlayer()).willReturn(player5);
        given(player5.getNextPlayer()).willReturn(player3);
        given(player3.getName()).willReturn("Player 3");
        given(player4.getName()).willReturn("Player 4");
        given(player5.getName()).willReturn("Player 5");
        given(user3.getName()).willReturn("Player 3");
        given(user4.getName()).willReturn("Player 4");
        given(user5.getName()).willReturn("Player 5");
        given(message.getContentRaw()).willReturn("remee kick Player 4");
        given(eventReaction.getReactionEmote()).willReturn(reactionEmote);
        given(eventReaction.getMessageId()).willReturn("1222");
        given(reactionEmote.getAsCodepoints()).willReturn(ICON_X);
        given(channel1.deleteMessageById(anyString())).willReturn(deleteMessage);
        testgame1.getVoteList().add(player4);
        testgame1.joinGame(player3);
        testgame1.joinGame(player4);
        testgame1.joinGame(player5);
        testgame1.setPlayerWhoGetsTurn(player3);
        testgame1.startGame();
        queryKick.onMessageReactionAdd(eventReaction);
    }


}
