package id.ac.ui.cs.remeebot.core.listener;

import id.ac.ui.cs.remeebot.core.game.gameplay.Game;
import id.ac.ui.cs.remeebot.core.game.gameplay.KartuSetanGame;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Map;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

public class QueryPlayTest {
    private QueryPlay queryPlay;
    private Game game;

    @Mock
    private GuildMessageReceivedEvent event;

    @Mock
    private MessageReactionAddEvent eventReaction;

    @Mock
    private Message message;

    @Mock
    private Member member;

    @Mock
    private Game gameMock;

    @Mock
    private User user;

    @Mock
    private Guild guild;

    @Mock
    private Map<Guild, Game> listGame = Game.listGame;

    @Mock
    private TextChannel channel;

    @Mock
    private MessageAction messageAction;

    @Mock
    private MessageReactionAddEvent emoteReaction;

    @Mock
    private RestAction<Void> addReaction1;

    @Mock
    private RestAction<Void> addReaction2;

    @Mock
    private MessageReaction.ReactionEmote emote;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        createGameAndGetPlayer();
        queryPlay = new QueryPlay();
    }

    public void createGameAndGetPlayer() {
        given(eventReaction.getTextChannel()).willReturn(channel);
        game = new KartuSetanGame(eventReaction);
    }

    @Test
    public void testPlayRemee() {
        given(event.getMessage()).willReturn(message);
        given(message.getContentRaw()).willReturn("remee play");
        given(event.getAuthor()).willReturn(user);
        given(event.getAuthor().getName()).willReturn("dummy");
        given(event.getAuthor().isBot()).willReturn(false);
        given(channel.sendMessage(anyString())).willReturn(messageAction);
        given(event.getGuild()).willReturn(guild);
        given(event.getChannel()).willReturn(channel);
        given(listGame.containsKey(guild)).willReturn(true);
        given(message.addReaction("1️⃣")).willReturn(addReaction1);
        given(message.addReaction("2️⃣")).willReturn(addReaction2);
        queryPlay.onGuildMessageReceived(event);
    }

    @Test
    public void testChoosePlay() {
        given(emoteReaction.getMember()).willReturn(member);
        given(member.getUser()).willReturn(user);
        given(user.isBot()).willReturn(false);
        given(channel.sendMessage(anyString())).willReturn(messageAction);
        given(event.getGuild()).willReturn(guild);
        given(emoteReaction.getReactionEmote()).willReturn(emote);
        given(emote.getAsCodepoints()).willReturn("U+32U+fe0fU+20e3");
        given(emoteReaction.getChannel()).willReturn(channel);
        given(listGame.containsKey(guild)).willReturn(true);
        given(listGame.put(guild, gameMock)).willReturn(game);
        given(gameMock.getGameName()).willReturn("Kartu 41");
        queryPlay.onMessageReactionAdd(emoteReaction);
    }

    @Test
    public void testChoosePlayAnotherGame() {
        Game.listGame.remove(event.getGuild());
        given(emoteReaction.getMember()).willReturn(member);
        given(member.getUser()).willReturn(user);
        given(user.isBot()).willReturn(false);
        given(channel.sendMessage(anyString())).willReturn(messageAction);
        given(event.getGuild()).willReturn(guild);
        given(emoteReaction.getReactionEmote()).willReturn(emote);
        given(emote.getAsCodepoints()).willReturn("U+31U+fe0fU+20e3");
        given(emoteReaction.getChannel()).willReturn(channel);
        given(listGame.containsKey(guild)).willReturn(true);
        given(listGame.put(guild, gameMock)).willReturn(game);
        given(gameMock.getGameName()).willReturn("Kartu Setan");
        queryPlay.onMessageReactionAdd(emoteReaction);
    }
}
