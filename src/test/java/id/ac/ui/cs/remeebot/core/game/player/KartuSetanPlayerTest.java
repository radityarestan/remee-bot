package id.ac.ui.cs.remeebot.core.game.player;

import id.ac.ui.cs.remeebot.core.game.card.*;
import id.ac.ui.cs.remeebot.core.game.gameplay.Game;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashSet;

import static id.ac.ui.cs.remeebot.core.game.ConstantEmote.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

public class KartuSetanPlayerTest {
    private Class<?> kartuSetanPlayerClass;
    private Player kartuSetanPlayer;


    @Mock
    private User user1;

    @Mock
    private Player player2;

    @Mock
    private Game game;

    @Mock
    private TextChannel channel;

    @Mock
    private MessageAction messageAction;


    @BeforeEach
    public void setUp() throws Exception {
        kartuSetanPlayerClass = Class.forName("id.ac.ui.cs.remeebot.core.game.player.KartuSetanPlayer");
        MockitoAnnotations.initMocks(this);
        given(user1.getName()).willReturn("Player 1");
        kartuSetanPlayer = new KartuSetanPlayer(user1, game);
        initCard();
    }

    public void initCard() {
        Card card1 = new Spade("2");
        Card card2 = new Club("3");
        Card card3 = new Diamond("4");
        Card card4 = new Heart("5");
        kartuSetanPlayer.getCard(card1);
        kartuSetanPlayer.getCard(card2);
        kartuSetanPlayer.getCard(card3);
        kartuSetanPlayer.getCard(card4);
    }

    @Test
    public void testKartuSetanIsConcreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(kartuSetanPlayerClass.getModifiers()));
    }

    @Test
    public void testKartuSetanPlayerIsAPlayer() throws Exception {
        Class<?> parentClass = kartuSetanPlayerClass.getSuperclass();
        assertEquals("id.ac.ui.cs.remeebot.core.game.player.Player", parentClass.getName());
    }

    @Test
    public void testMethodRun() throws Exception {
        given(player2.removeCard(anyInt())).willReturn(new Spade("3"));
        given(game.getChannel()).willReturn(channel);
        given(channel.sendMessage(anyString())).willReturn(messageAction);
        kartuSetanPlayer.setNextPlayer(player2);
        kartuSetanPlayer.run(ICON_A);
    }

    @Test
    public void testSetSorted() throws Exception {
        kartuSetanPlayer.setSorted(true);
        assertEquals(true, kartuSetanPlayer.getSorted());
    }

    @Test
    public void testUserName() throws Exception {
        assertEquals("Player 1", kartuSetanPlayer.getName());
    }

    @Test
    public void testListCardSize() throws Exception {
        assertEquals(4, kartuSetanPlayer.getListCard().size());
    }

    @Test
    public void testCheckCard() throws Exception {
        ((KartuSetanPlayer) kartuSetanPlayer).checkCard();
        assertTrue(true);
    }

    @Test
    public void testRemoveCard() throws Exception {
        kartuSetanPlayer.removeCard(0);
        assertEquals(3, kartuSetanPlayer.getListCard().size());
    }

    @Test
    public void testGetDistIfJokerDoesntExist() throws Exception {
        String result = "```This is Your Card\n"
                + ICON_SPADE + ":2 \n" + ICON_HEART + ":5 \n" +
                ICON_DIAMOND + ":4 \n" + ICON_CLUB + ":3 ```";

        assertEquals(result, kartuSetanPlayer.getDist());
    }

    @Test
    public void testGetDistIfJokerExist() throws Exception {
        String result = "```This is Your Card\n" +
                ICON_SPADE + ":2 \n" + ICON_HEART + ":5 \n" +
                ICON_DIAMOND + ":4 \n" + ICON_CLUB + ":3 \n" +
                ICON_JOKER + "Kamu dapat KARTU SETAN```";

        Card jokerCard = new Joker("Joker");
        kartuSetanPlayer.getCard(jokerCard);
        assertEquals(result, kartuSetanPlayer.getDist());
    }

    @Test
    public void testMethodAnnounceDumped() throws Exception {
        given(game.getChannel()).willReturn(channel);
        given(channel.sendMessage(anyString())).willReturn(messageAction);

        Field dumpedCard = kartuSetanPlayer.getClass().getDeclaredField("dumpedCard");
        dumpedCard.setAccessible(true);

        Method announceDumped = kartuSetanPlayer.getClass().getDeclaredMethod("announceDumped");
        announceDumped.setAccessible(true);

        dumpedCard.set(kartuSetanPlayer, new HashSet<Card>(0));

        announceDumped.invoke(kartuSetanPlayer);
    }

}