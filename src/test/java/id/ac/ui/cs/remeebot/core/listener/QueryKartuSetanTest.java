package id.ac.ui.cs.remeebot.core.listener;

import id.ac.ui.cs.remeebot.core.game.card.Card;
import id.ac.ui.cs.remeebot.core.game.card.Heart;
import id.ac.ui.cs.remeebot.core.game.card.Spade;
import id.ac.ui.cs.remeebot.core.game.gameplay.Game;
import id.ac.ui.cs.remeebot.core.game.gameplay.KartuSetanGame;
import id.ac.ui.cs.remeebot.core.game.player.KartuSetanPlayer;
import id.ac.ui.cs.remeebot.core.game.player.Player;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.requests.restaction.AuditableRestAction;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Map;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static id.ac.ui.cs.remeebot.core.game.ConstantEmote.ICON_A;

public class QueryKartuSetanTest {
    private QueryKartuSetan queryKartuSetan;
    private Game game;

    @Mock
    private MessageReactionAddEvent event;

    @Mock
    private GuildMessageReactionAddEvent eventGuild;

    @Mock
    private Guild guild;

    @Mock
    private TextChannel channel;

    @Mock
    private Map<Guild, Game> listGame = Game.listGame;

    @Mock
    private MessageReaction.ReactionEmote reactionEmote;

    @Mock
    private User user1;

    @Mock
    private User user2;

    @Mock
    private Player player1;

    @Mock
    private Player player2;

    @Mock
    private RestAction<PrivateChannel> privateChannelRestAction;

    @Mock
    private MessageAction messageAction;

    @Mock
    AuditableRestAction deleteMessage;


    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        setUpGameAndPlayer();
        queryKartuSetan = new QueryKartuSetan();
    }

    public void setUpGameAndPlayer() {
        given(event.getTextChannel()).willReturn(channel);
        game = new KartuSetanGame(event);
        Game.listGame.put(guild, game);

        game.joinGame(player1);
        game.joinGame(player2);
    }

    @Test
    public void testChooseACard() throws Exception {
        game.setPlayerWhoGetsTurn(player1);
        given(eventGuild.getGuild()).willReturn(guild);
        given(listGame.containsKey(guild)).willReturn(true);
        given(listGame.get(guild)).willReturn(game);
        given(player1.getNextPlayer()).willReturn(player2);
        given(player1.getUser()).willReturn(user1);
        given(eventGuild.getUser()).willReturn(user1);
        given(eventGuild.getReactionEmote()).willReturn(reactionEmote);
        given(reactionEmote.getAsCodepoints()).willReturn(ICON_A);
        given(player1.getNextPlayer()).willReturn(player2);
        given(player2.getNextPlayer()).willReturn(player1);
        given(player2.getUser()).willReturn(user2);
        given(user1.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user2.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(player1.getName()).willReturn("Player 1");
        given(player2.getName()).willReturn("Player 2");
        given(channel.sendMessage(anyString())).willReturn(messageAction);
        given(eventGuild.getMessageId()).willReturn("message ID");
        given(channel.deleteMessageById(anyString())).willReturn(deleteMessage);
        queryKartuSetan.onGuildMessageReactionAdd(eventGuild);
    }

    @Test
    public void testChooseACardIfPlayerDoesntExist() throws Exception {
        given(eventGuild.getGuild()).willReturn(guild);
        given(listGame.containsKey(guild)).willReturn(true);
        given(listGame.get(guild)).willReturn(game);
        given(player1.getNextPlayer()).willReturn(player2);
        given(player1.getUser()).willReturn(user1);
        given(eventGuild.getUser()).willReturn(user1);
        given(eventGuild.getReactionEmote()).willReturn(reactionEmote);
        given(reactionEmote.getAsCodepoints()).willReturn(ICON_A);
        given(player1.getNextPlayer()).willReturn(player2);
        given(player2.getNextPlayer()).willReturn(player1);
        given(player2.getUser()).willReturn(user2);
        given(user1.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user2.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(player1.getName()).willReturn("Player 1");
        given(player2.getName()).willReturn("Player 2");
        given(channel.sendMessage(anyString())).willReturn(messageAction);
        given(eventGuild.getMessageId()).willReturn("message ID");
        given(channel.deleteMessageById(anyString())).willReturn(deleteMessage);
        queryKartuSetan.onGuildMessageReactionAdd(eventGuild);
    }
}