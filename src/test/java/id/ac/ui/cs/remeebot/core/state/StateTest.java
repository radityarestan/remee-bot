package id.ac.ui.cs.remeebot.core.state;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class StateTest {
    private Class<?> stateClass;

    @BeforeEach
    public void setUp() throws Exception {
        stateClass = Class.forName("id.ac.ui.cs.remeebot.core.state.State");
    }

    @Test
    public void testStateIsPublicAndAbstractClass() throws Exception {
        int methodModifier = stateClass.getModifiers();
        assertTrue(Modifier.isAbstract(methodModifier));
        assertTrue(Modifier.isPublic(methodModifier));
    }
}
