package id.ac.ui.cs.remeebot.core.game.card;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DiamondTest {
    private Class<?> diamondClass;
    private Diamond diamond;

    @BeforeEach
    public void setUp() throws Exception {
        diamondClass = Class.forName("id.ac.ui.cs.remeebot.core.game.card.Diamond");
        diamond = new Diamond("test");
    }

    @Test
    public void testGetTypeIsCorrect() {
        assertEquals("Diamond", diamond.getType());
    }

    @Test
    public void testGetNameAndTypeIsCorrect() {
        assertEquals("test Diamond", diamond.getNameAndType());
    }

    @Test
    public void testToString() {
        assertEquals("test\u2666", diamond.toString());
    }
}
