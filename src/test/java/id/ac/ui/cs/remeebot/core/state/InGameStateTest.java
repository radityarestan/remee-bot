package id.ac.ui.cs.remeebot.core.state;

import id.ac.ui.cs.remeebot.core.game.gameplay.Kartu41Game;
import id.ac.ui.cs.remeebot.core.game.player.Kartu41Player;
import id.ac.ui.cs.remeebot.core.game.player.Player;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

public class InGameStateTest {
    private Class<?> inGameState;
    private Kartu41Game game;

    @Mock
    private MessageReactionAddEvent event;

    @Mock
    private TextChannel channel;

    @Mock
    private User user;

    @Mock
    private MessageAction messageAction;

    @Mock
    private RestAction<PrivateChannel> privateChannelRestAction;

    @BeforeEach
    public void setUp() throws Exception {
        inGameState = Class.forName("id.ac.ui.cs.remeebot.core.state.InGameState");
        MockitoAnnotations.initMocks(this);
        given(event.getTextChannel()).willReturn(channel);
        game = new Kartu41Game(event);

    }

    @Test
    public void testJoinInGameState() {
        given(user.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(channel.sendMessage(anyString())).willReturn(messageAction);
        Player player1 = new Kartu41Player(user);
        Player player2 = new Kartu41Player(user);
        Player player3 = new Kartu41Player(user);
        Player player4 = new Kartu41Player(user);
        game.joinGame(player1);
        game.joinGame(player2);
        game.joinGame(player3);
        game.setPlayerWhoGetsTurn(player1);
        game.startGame();
        assertFalse(game.joinGame(player4));
    }

    @Test
    public void testStartInGameState() {
        given(user.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(channel.sendMessage(anyString())).willReturn(messageAction);
        Player player1 = new Kartu41Player(user);
        Player player2 = new Kartu41Player(user);
        game.joinGame(player1);
        game.joinGame(player2);
        game.setPlayerWhoGetsTurn(player1);
        game.startGame();
        assertFalse(game.startGame());
    }
}
