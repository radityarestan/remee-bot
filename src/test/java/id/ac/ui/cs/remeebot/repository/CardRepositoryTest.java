package id.ac.ui.cs.remeebot.repository;

import id.ac.ui.cs.remeebot.core.game.card.Spade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CardRepositoryTest {
    private Class<?> cardRepositoryClass;
    private CardRepository cardRepository;

    @BeforeEach
    public void setUp() throws Exception {
        cardRepositoryClass = Class.forName("id.ac.ui.cs.remeebot.repository.CardRepository");
    }

    @Test
    public void testGetCardList() {
        cardRepository = new CardRepository();
        assertTrue(cardRepository.getCardList().add(new Spade("Dummy")));
    }

    @Test
    public void testAddCard() {
        cardRepository = new CardRepository();
        assertTrue(cardRepository.addCard(new Spade("Dummy")));
    }


}


