package id.ac.ui.cs.remeebot.core.listener;

import id.ac.ui.cs.remeebot.core.game.gameplay.Game;
import id.ac.ui.cs.remeebot.core.game.gameplay.Kartu41Game;
import id.ac.ui.cs.remeebot.core.game.gameplay.KartuSetanGame;
import id.ac.ui.cs.remeebot.core.game.player.Kartu41Player;
import id.ac.ui.cs.remeebot.core.game.player.KartuSetanPlayer;
import id.ac.ui.cs.remeebot.core.game.player.Player;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Map;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

public class QueryJoinTest {
    private QueryJoin queryJoin;
    private Game game;

    @Mock
    private MessageReactionAddEvent eventReaction;

    @Mock
    private Map<Guild, Game> listGame = Game.listGame;

    @Mock
    private Guild guild;

    @Mock
    private TextChannel channel;

    @Mock
    private User user;

    @Mock
    private User anotherUser;

    @Mock
    private Member member;

    @Mock
    MessageReaction.ReactionEmote reactionEmote;

    @Mock
    MessageAction messageAction;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        createGameAndGetPlayer();
        queryJoin = new QueryJoin();
    }

    public void createGameAndGetPlayer() {
        given(eventReaction.getTextChannel()).willReturn(channel);
        game = new KartuSetanGame(eventReaction);
        Game.listGame.put(guild, game);
        Player player = new KartuSetanPlayer(user, game);
        game.joinGame(player);
    }

    @Test
    public void testJoinRemee() throws Exception {
        given(eventReaction.getMember()).willReturn(member);
        given(member.getUser()).willReturn(user);
        given(user.isBot()).willReturn(false);
        given(eventReaction.getGuild()).willReturn(guild);
        given(listGame.containsKey(guild)).willReturn(true);
        given(eventReaction.getReactionEmote()).willReturn(reactionEmote);
        given(reactionEmote.getAsCodepoints()).willReturn("U+270B");
        given(listGame.get(guild)).willReturn(game);
        given(eventReaction.getUser()).willReturn(user);

        queryJoin.onMessageReactionAdd(eventReaction);
    }

    @Test
    public void testJoinRemeeIfPlayerNotExist() throws Exception {
        given(eventReaction.getMember()).willReturn(member);
        given(member.getUser()).willReturn(user);
        given(user.isBot()).willReturn(false);
        given(eventReaction.getGuild()).willReturn(guild);
        given(listGame.containsKey(guild)).willReturn(true);
        given(eventReaction.getReactionEmote()).willReturn(reactionEmote);
        given(reactionEmote.getAsCodepoints()).willReturn("U+270B");
        given(listGame.get(guild)).willReturn(game);
        given(eventReaction.getUser()).willReturn(anotherUser);
        given(eventReaction.getChannel()).willReturn(channel);
        given(anotherUser.getName()).willReturn("dummy user");
        given(channel.sendMessage(anyString())).willReturn(messageAction);

        queryJoin.onMessageReactionAdd(eventReaction);
    }

    @Test
    public void testJoinRemeeIfPlayerNotExistAnotherGame() throws Exception {
        Game.listGame.remove(guild);
        game = new Kartu41Game(eventReaction);
        Game.listGame.put(guild, game);
        Player player = new Kartu41Player(user);
        game.joinGame(player);

        given(eventReaction.getMember()).willReturn(member);
        given(member.getUser()).willReturn(user);
        given(user.isBot()).willReturn(false);
        given(eventReaction.getGuild()).willReturn(guild);
        given(listGame.containsKey(guild)).willReturn(true);
        given(eventReaction.getReactionEmote()).willReturn(reactionEmote);
        given(reactionEmote.getAsCodepoints()).willReturn("U+270B");
        given(listGame.get(guild)).willReturn(game);
        given(eventReaction.getUser()).willReturn(anotherUser);
        given(eventReaction.getChannel()).willReturn(channel);
        given(anotherUser.getName()).willReturn("dummy user");
        given(channel.sendMessage(anyString())).willReturn(messageAction);

        queryJoin.onMessageReactionAdd(eventReaction);
    }
}
