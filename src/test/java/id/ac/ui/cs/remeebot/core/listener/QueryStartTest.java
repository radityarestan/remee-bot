package id.ac.ui.cs.remeebot.core.listener;

import id.ac.ui.cs.remeebot.core.game.gameplay.Game;
import id.ac.ui.cs.remeebot.core.game.gameplay.KartuSetanGame;
import id.ac.ui.cs.remeebot.core.game.player.KartuSetanPlayer;
import id.ac.ui.cs.remeebot.core.game.player.Player;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Map;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

public class QueryStartTest {
    private QueryStart queryStart;
    private Game game;

    @Mock
    private GuildMessageReceivedEvent event;

    @Mock
    private MessageReactionAddEvent eventReaction;

    @Mock
    private Message message;

    @Mock
    private Member member;

    @Mock
    private User user;

    @Mock
    private KartuSetanPlayer player;

    @Mock
    private User user2;

    @Mock
    private KartuSetanPlayer player2;

    @Mock
    private Guild guild;

    @Mock
    private Map<Guild, Game> listGame = Game.listGame;

    @Mock
    private TextChannel channel;

    @Mock
    private MessageAction messageAction;

    @Mock
    private RestAction<PrivateChannel> privateChannelRestAction;


    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        createGameAndGetPlayer();
        queryStart = new QueryStart();
    }

    public void createGameAndGetPlayer() {
        given(eventReaction.getTextChannel()).willReturn(channel);
        game = new KartuSetanGame(eventReaction);
        Game.listGame.put(guild, game);
        game.joinGame(player);
        game.joinGame(player2);
    }

    @Test
    public void testStartRemee() {
        given(event.getMessage()).willReturn(message);
        given(message.getContentRaw()).willReturn("remee start");
        given(event.getMember()).willReturn(member);
        given(member.getUser()).willReturn(user);
        given(user.isBot()).willReturn(false);
        given(event.getGuild()).willReturn(guild);
        given(listGame.containsKey(guild)).willReturn(true);
        given(listGame.get(guild)).willReturn(game);
        given(event.getChannel()).willReturn(channel);
        given(channel.sendMessage(anyString())).willReturn(messageAction);
        given(player.getUser()).willReturn(user);
        given(player2.getUser()).willReturn(user2);
        given(user.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user2.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(player.getNextPlayer()).willReturn(player2);
        given(player2.getNextPlayer()).willReturn(player);
        given(player.getName()).willReturn("Player 1");
        given(player2.getName()).willReturn("Player 2");
        queryStart.onGuildMessageReceived(event);
    }
}