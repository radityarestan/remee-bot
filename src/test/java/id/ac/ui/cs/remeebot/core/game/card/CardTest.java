package id.ac.ui.cs.remeebot.core.game.card;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class CardTest {
    private Class<?> cardClass;
    private Card card;

    @BeforeEach
    public void setUp() throws Exception {
        cardClass = Class.forName("id.ac.ui.cs.remeebot.core.game.card.Card");
        card = new Card("test");
    }

    @Test
    public void testCardIsConcreteAndPublic() {
        assertFalse(Modifier.isAbstract(cardClass.getModifiers()));
        assertTrue(Modifier.isPublic(cardClass.getModifiers()));
    }

    @Test
    public void testNameIsCorrect() {
        assertEquals("test", card.getName());
    }

    @Test
    public void testGetIntValueIsCorrect() {
        card = new Card("K");
        assertEquals(10, card.getValue());
        card = new Card("Q");
        assertEquals(10, card.getValue());
        card = new Card("J");
        assertEquals(10, card.getValue());
        card = new Card("A");
        assertEquals(11, card.getValue());
        card = new Card("2");
        assertEquals(2, card.getValue());
        card = new Card("3");
        assertEquals(3, card.getValue());
        card = new Card("4");
        assertEquals(4, card.getValue());
        card = new Card("5");
        assertEquals(5, card.getValue());
        card = new Card("6");
        assertEquals(6, card.getValue());
        card = new Card("7");
        assertEquals(7, card.getValue());
        card = new Card("8");
        assertEquals(8, card.getValue());
        card = new Card("9");
        assertEquals(9, card.getValue());
        card = new Card("10");
        assertEquals(10, card.getValue());
    }
}
