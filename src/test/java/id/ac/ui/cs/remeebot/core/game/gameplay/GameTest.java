package id.ac.ui.cs.remeebot.core.game.gameplay;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class GameTest {
    private Class<?> gameClass;

    @BeforeEach
    public void setUp() throws Exception {
        gameClass = Class.forName("id.ac.ui.cs.remeebot.core.game.gameplay.Game");
    }

    @Test
    public void testGameIsPublicAndAbstractClass() throws Exception {
        int methodModifier = gameClass.getModifiers();
        assertTrue(Modifier.isAbstract(methodModifier));
        assertTrue(Modifier.isPublic(methodModifier));
    }
}
