package id.ac.ui.cs.remeebot.core.state;

import id.ac.ui.cs.remeebot.core.game.gameplay.Kartu41Game;
import id.ac.ui.cs.remeebot.core.game.player.Kartu41Player;
import id.ac.ui.cs.remeebot.core.game.player.Player;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

public class GamePreparationStateTest {
    private Class<?> gamePreparationState;
    private Kartu41Game game;

    @Mock
    private MessageReactionAddEvent event;

    @Mock
    private TextChannel channel;


    @Mock
    private User user1;

    @Mock
    private User user2;

    @Mock
    private User user3;

    @Mock
    private Player player1;

    @Mock
    private Player player2;

    @Mock
    private Player player3;


    @Mock
    private MessageAction messageAction;

    @Mock
    private RestAction<PrivateChannel> privateChannelRestAction;


    @BeforeEach
    public void setUp() throws Exception {
        gamePreparationState = Class.forName("id.ac.ui.cs.remeebot.core.state.GamePreparationState");
        MockitoAnnotations.initMocks(this);
        given(event.getTextChannel()).willReturn(channel);
        game = new Kartu41Game(event);
    }

    @Test
    public void testJoinPreparationStateCanJoin() {
        assertTrue(game.joinGame(player1));
    }

    @Test
    public void testJoinPreparationStateCannotJoin() {
        given(channel.sendMessage(anyString())).willReturn(messageAction);
        game.joinGame(player1);
        game.joinGame(player1);
        game.joinGame(player1);
        game.joinGame(player1);
        assertFalse(game.joinGame(player1));
    }

    @Test
    public void testStartPreparationState() {
        game.setPlayerWhoGetsTurn(player1);
        given(player1.getUser()).willReturn(user1);
        given(player2.getUser()).willReturn(user2);
        given(player3.getUser()).willReturn(user3);
        given(user1.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user2.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user3.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(player1.getNextPlayer()).willReturn(player2);
        given(player2.getNextPlayer()).willReturn(player3);
        given(player3.getNextPlayer()).willReturn(player1);
        given(player1.getName()).willReturn("Player 1");
        given(player2.getName()).willReturn("Player 2");
        given(player3.getName()).willReturn("Player 3");
        given(channel.sendMessage(anyString())).willReturn(messageAction);
        game.joinGame(player1);
        game.joinGame(player2);
        game.joinGame(player3);
        assertTrue(game.startGame());
    }
}







