package id.ac.ui.cs.remeebot.core.game.player;

import id.ac.ui.cs.remeebot.core.game.card.*;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.lang.reflect.Modifier;
import java.util.Stack;

import static id.ac.ui.cs.remeebot.core.game.ConstantEmote.ICON_B;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.BDDMockito.given;

public class Kartu41PlayerTest {
    private Class<?> kartu41PlayerClass;
    private Kartu41Player kartu41Player;

    @Mock
    private User user1;

    @Mock
    private User user2;

    @Mock
    private User user3;

    @Mock
    private User user4;

    @Mock
    private User user5;

    @Mock
    private Stack<Card> dumpedCard;


    @BeforeEach
    public void setUp() throws Exception {
        kartu41PlayerClass = Class.forName("id.ac.ui.cs.remeebot.core.game.player.Kartu41Player");
        MockitoAnnotations.initMocks(this);
        given(user1.getName()).willReturn("Player 1");
        kartu41Player = new Kartu41Player(user1);
        Kartu41Player player2 = new Kartu41Player(user2);
        Card card1 = new Spade("3");
        kartu41Player.getDumpedCard().push(card1);
        player2.getDumpedCard().push(card1);
        kartu41Player.setNextPlayer(player2);
        initCard();
    }

    public void initCard() {
        Card card1 = new Spade("2");
        Card card2 = new Club("2");
        Card card3 = new Diamond("2");
        Card card4 = new Heart("2");
        kartu41Player.getCard(card1);
        kartu41Player.getCard(card2);
        kartu41Player.getCard(card3);
        kartu41Player.getCard(card4);
    }

    @Test
    public void testKartu41IsConcreteClass() {
        assertFalse(Modifier.isAbstract(kartu41PlayerClass.getModifiers()));
    }

    @Test
    public void testKartu41PlayerIsAPlayer() {
        Class<?> parentClass = kartu41PlayerClass.getSuperclass();
        assertEquals("id.ac.ui.cs.remeebot.core.game.player.Player", parentClass.getName());
    }

    @Test
    public void testMethodRun() {
        kartu41Player.run(ICON_B);
    }

    @Test
    public void testRemoveCard() throws Exception {
        kartu41Player.removeCard(0);
        assertEquals(3, kartu41Player.getListCard().size());
    }

    @Test
    public void check41Is41() {
        kartu41Player = new Kartu41Player(user3);
        kartu41Player.getCard(new Spade("A"));
        kartu41Player.getCard(new Spade("K"));
        kartu41Player.getCard(new Spade("Q"));
        kartu41Player.getCard(new Spade("J"));
        assertEquals(true, kartu41Player.check41());
    }

    @Test
    public void check41IsNot41() {
        kartu41Player = new Kartu41Player(user3);
        kartu41Player.getCard(new Spade("2"));
        kartu41Player.getCard(new Heart("3"));
        kartu41Player.getCard(new Club("5"));
        kartu41Player.getCard(new Diamond("6"));
        assertEquals(false, kartu41Player.check41());
    }

    @Test
    public void countRestTestIfSpade() {
        kartu41Player = new Kartu41Player(user3);
        kartu41Player.getCard(new Spade("A"));
        kartu41Player.getCard(new Heart("1"));
        kartu41Player.getCard(new Club("1"));
        kartu41Player.getCard(new Diamond("1"));
        kartu41Player.countRest();
        assertEquals(8, kartu41Player.getFinalPoints());
    }

    @Test
    public void countRestTestIfHeart() {
        kartu41Player = new Kartu41Player(user3);
        kartu41Player.getCard(new Heart("A"));
        kartu41Player.getCard(new Spade("1"));
        kartu41Player.getCard(new Club("1"));
        kartu41Player.getCard(new Diamond("1"));
        kartu41Player.countRest();
        assertEquals(8, kartu41Player.getFinalPoints());
    }

    @Test
    public void countRestTestIfClub() {
        kartu41Player = new Kartu41Player(user3);
        kartu41Player.getCard(new Club("A"));
        kartu41Player.getCard(new Spade("1"));
        kartu41Player.getCard(new Heart("1"));
        kartu41Player.getCard(new Diamond("1"));
        kartu41Player.countRest();
        assertEquals(8, kartu41Player.getFinalPoints());
    }

    @Test
    public void countRestTestIfDiamond() {
        kartu41Player = new Kartu41Player(user3);
        kartu41Player.getCard(new Diamond("A"));
        kartu41Player.getCard(new Spade("1"));
        kartu41Player.getCard(new Heart("1"));
        kartu41Player.getCard(new Club("1"));
        kartu41Player.countRest();
        assertEquals(8, kartu41Player.getFinalPoints());
    }

    @Test
    public void compareToTest() {
        Kartu41Player playerOne = new Kartu41Player(user4);
        Kartu41Player playerTwo = new Kartu41Player(user5);
        playerOne.setFinalPoints(1);
        playerTwo.setFinalPoints(2);
        assertEquals(1, playerOne.compareTo(playerTwo));
        playerOne.setFinalPoints(2);
        playerTwo.setFinalPoints(1);
        assertEquals(-1, playerOne.compareTo(playerTwo));
        playerOne.setFinalPoints(1);
        playerTwo.setFinalPoints(1);
        assertEquals(0, playerOne.compareTo(playerTwo));
    }
}
