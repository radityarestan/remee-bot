package id.ac.ui.cs.remeebot.core.game.player;

import id.ac.ui.cs.remeebot.core.game.card.Card;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;

public class playerTest {
    private Class<?> playerClass;
    Player player;

    @Mock
    private Card card;

    @Mock
    private User user;


    @BeforeEach
    public void setUp() throws Exception {
        playerClass = Class.forName("id.ac.ui.cs.remeebot.core.game.player.Player");
        MockitoAnnotations.initMocks(this);
        given(user.getName()).willReturn("player1");
        player = new Kartu41Player(user);
    }

    @Test
    public void testPlayerIsAbstractAndPublic() {
        assertTrue(Modifier.isAbstract(playerClass.getModifiers()));
        assertTrue(Modifier.isPublic(playerClass.getModifiers()));
    }

    @Test
    public void testAddCard() {
        assertTrue(player.addCard(card));
    }
}
