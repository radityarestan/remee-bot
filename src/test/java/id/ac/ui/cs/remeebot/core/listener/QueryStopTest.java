package id.ac.ui.cs.remeebot.core.listener;

import id.ac.ui.cs.remeebot.core.game.gameplay.Game;
import id.ac.ui.cs.remeebot.core.game.gameplay.KartuSetanGame;
import id.ac.ui.cs.remeebot.core.game.player.KartuSetanPlayer;
import id.ac.ui.cs.remeebot.core.game.player.Player;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Map;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

public class QueryStopTest {
    private QueryStop queryStop;
    private Game game;

    @Mock
    private Guild guild;

    @Mock
    private GuildMessageReceivedEvent event;

    @Mock
    private MessageReactionAddEvent eventReaction;

    @Mock
    private TextChannel channel;

    @Mock
    private User user;

    @Mock
    private Map<Guild, Game> listGame = Game.listGame;

    @Mock
    private Message message;

    @Mock
    private Member member;

    @Mock
    private MessageAction messageAction;

    @Mock
    private ArrayList<Player> mockPlayers;

    @Mock
    private Player mockPlayer;

    @Mock
    private Game mockGame;


    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        createGameAndGetPlayer();
        queryStop = new QueryStop();
    }

    public void createGameAndGetPlayer() throws NoSuchFieldException, IllegalAccessException {
        given(eventReaction.getTextChannel()).willReturn(channel);
        game = new KartuSetanGame(eventReaction);
        Game.listGame.put(guild, game);
        Player player = new KartuSetanPlayer(user, game);

//        Field nameField = ((KartuSetanPlayer) player).getClass().getDeclaredField("name");
//        nameField.setAccessible(true);
//        nameField.set(player, "dummy");

        game.joinGame(player);
    }

    @Test
    public void testStopRemee() {

        given(event.getAuthor()).willReturn(user);
        given(user.isBot()).willReturn(false);
        given(event.getGuild()).willReturn(guild);
        given(listGame.containsKey(guild)).willReturn(true);

        given(event.getMessage()).willReturn(message);
        given(message.getContentRaw()).willReturn("remee stop");
        given(event.getMember()).willReturn(member);
        given(member.getUser()).willReturn(user);
        given(user.getName()).willReturn("dummy");

        given(listGame.get(guild)).willReturn(mockGame);
        given(mockGame.getPlayers()).willReturn(mockPlayers);
        given(mockPlayers.size()).willReturn(1);
        given(mockPlayers.get(0)).willReturn(mockPlayer);
        when(mockPlayer.getName()).thenReturn("dummy");

        given(event.getChannel()).willReturn(channel);
        given(channel.sendMessage(anyString())).willReturn(messageAction);
        queryStop.onGuildMessageReceived(event);
    }
}
