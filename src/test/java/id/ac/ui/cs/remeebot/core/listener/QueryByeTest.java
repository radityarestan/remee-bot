package id.ac.ui.cs.remeebot.core.listener;

import id.ac.ui.cs.remeebot.core.game.gameplay.Game;
import id.ac.ui.cs.remeebot.core.game.gameplay.KartuSetanGame;
import id.ac.ui.cs.remeebot.core.game.player.KartuSetanPlayer;
import id.ac.ui.cs.remeebot.core.game.player.Player;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Map;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

public class QueryByeTest {
    private QueryBye queryBye;
    private Game game;

    @Mock
    private GuildMessageReceivedEvent event;

    @Mock
    private MessageReactionAddEvent eventReaction;

    @Mock
    private Message message;

    @Mock
    private Member member;

    @Mock
    private Game gameMock;

    @Mock
    private User user;

    @Mock
    private Guild guild;

    @Mock
    private Map<Guild, Game> listGame = Game.listGame;

    @Mock
    private TextChannel channel;

    @Mock
    private MessageAction messageAction;

    @Mock
    private RestAction<Void> restAction;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        createGameAndGetPlayer();
        queryBye = new QueryBye();
    }

    public void createGameAndGetPlayer() {
        given(eventReaction.getTextChannel()).willReturn(channel);
        game = new KartuSetanGame(eventReaction);
        Game.listGame.put(guild, game);
        Player player = new KartuSetanPlayer(user, game);
        game.joinGame(player);
    }

    @Test
    public void testByeRemee() {
        given(event.getAuthor()).willReturn(user);
        given(user.isBot()).willReturn(false);
        given(event.getGuild()).willReturn(guild);
        given(listGame.containsKey(guild)).willReturn(true);
        given(event.getMessage()).willReturn(message);
        given(message.getContentRaw()).willReturn("remee bye");
        given(event.getMember()).willReturn(member);
        given(member.getUser()).willReturn(user);
        given(user.getName()).willReturn("dummy");
        given(event.getChannel()).willReturn(channel);
        given(channel.sendMessage(anyString())).willReturn(messageAction);
        given(guild.leave()).willReturn(restAction);
        queryBye.onGuildMessageReceived(event);
    }

}
