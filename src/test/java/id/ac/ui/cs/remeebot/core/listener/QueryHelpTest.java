package id.ac.ui.cs.remeebot.core.listener;

import id.ac.ui.cs.remeebot.core.game.gameplay.Game;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Map;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

public class QueryHelpTest {
    private QueryHelp queryHelp;

    @Mock
    private GuildMessageReceivedEvent event;

    @Mock
    private MessageReactionAddEvent eventReaction;

    @Mock
    private Message message;

    @Mock
    private Member member;

    @Mock
    private Game gameMock;

    @Mock
    private User user;

    @Mock
    private Guild guild;

    @Mock
    private Map<Guild, Game> listGame = Game.listGame;

    @Mock
    private TextChannel channel;

    @Mock
    private MessageAction messageAction;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        createGameAndGetPlayer();
        queryHelp = new QueryHelp();
    }

    public void createGameAndGetPlayer() {
        given(eventReaction.getTextChannel()).willReturn(channel);
    }

    @Test
    public void testHelpRemee() {
        given(event.getMessage()).willReturn(message);
        given(message.getContentRaw()).willReturn("remee help");
        given(event.getAuthor()).willReturn(user);
        given(event.getAuthor().isBot()).willReturn(false);
        given(channel.sendMessage(anyString())).willReturn(messageAction);
        given(event.getChannel()).willReturn(channel);
        queryHelp.onGuildMessageReceived(event);
    }
}
