package id.ac.ui.cs.remeebot.core.listener;

import id.ac.ui.cs.remeebot.core.game.card.Card;
import id.ac.ui.cs.remeebot.core.game.card.Heart;
import id.ac.ui.cs.remeebot.core.game.card.Spade;
import id.ac.ui.cs.remeebot.core.game.gameplay.Game;
import id.ac.ui.cs.remeebot.core.game.gameplay.Kartu41Game;
import id.ac.ui.cs.remeebot.core.game.gameplay.KartuSetanGame;
import id.ac.ui.cs.remeebot.core.game.player.Kartu41Player;
import id.ac.ui.cs.remeebot.core.game.player.KartuSetanPlayer;
import id.ac.ui.cs.remeebot.core.game.player.Player;
import id.ac.ui.cs.remeebot.core.state.InGameState;
import id.ac.ui.cs.remeebot.repository.CardRepository;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.requests.restaction.AuditableRestAction;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;
import java.util.Map;
import java.util.Stack;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

public class QueryLeaveTest {
    private QueryLeave queryLeave;
    private Game testgame1;
    private Game testgame2;
    private Game testgame3;

    @Mock
    private GuildMessageReceivedEvent event;

    @Mock
    private MessageReactionAddEvent eventReaction;

    @Mock
    private Message message;

    @Mock
    private Member member;

    @Mock
    private User user1;

    @Mock
    private User user2;

    @Mock
    private User user3;

    @Mock
    private User user4;

    @Mock
    private User user5;

    @Mock
    private Player player1;

    @Mock
    private Kartu41Player player2;

    @Mock
    private Kartu41Player player6;

    @Mock
    private KartuSetanPlayer player3;

    @Mock
    private KartuSetanPlayer player4;

    @Mock
    private  KartuSetanPlayer player5;

    @Mock
    private Guild guild;

    @Mock
    private Map<Guild, Game> listGame = Game.listGame;

    @Mock
    private TextChannel channel1;

    @Mock
    private MessageAction messageAction;

    @Mock
    private RestAction<PrivateChannel> privateChannelRestAction;

    @Mock
    private CardRepository cardRepository;

    @Mock
    private Stack<Card> deck;

    @Mock
    AuditableRestAction deleteMessage;

    @Mock
    private List<Card> cardList;

    @Mock
    private Card dummyCard;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        createGameAndGetPlayer();
        queryLeave = new QueryLeave();
    }

    public void createGameAndGetPlayer() {
        given(eventReaction.getTextChannel()).willReturn(channel1);
        testgame1 = new KartuSetanGame(eventReaction);
        testgame2 = new Kartu41Game(eventReaction);
        testgame3 = new KartuSetanGame(eventReaction);
    }

    @Test
    public void testLeaveKartuSetanGame() {
        Game.listGame.put(guild, testgame1);
        testgame1.setCurrentMessageId("123");
        given(event.getAuthor()).willReturn(user3);
        given(event.getMember()).willReturn(member);
        given(member.getUser()).willReturn(user3);
        given(user3.isBot()).willReturn(false);
        given(event.getGuild()).willReturn(guild);
        given(event.getMessage()).willReturn(message);
        given(message.getContentRaw()).willReturn("remee leave");
        given(player3.getUser()).willReturn(user3);
        given(player4.getUser()).willReturn(user4);
        given(player5.getUser()).willReturn(user5);
        given(event.getChannel()).willReturn(channel1);
        given(channel1.sendMessage(anyString())).willReturn(messageAction);
        given(user3.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user4.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user5.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(player3.getNextPlayer()).willReturn(player4);
        given(player4.getNextPlayer()).willReturn(player5);
        given(player5.getNextPlayer()).willReturn(player3);
        given(player3.getName()).willReturn("Player 3");
        given(player4.getName()).willReturn("Player 4");
        given(player5.getName()).willReturn("Player 5");
        given(channel1.deleteMessageById(anyString())).willReturn(deleteMessage);
        testgame1.joinGame(player3);
        testgame1.joinGame(player4);
        testgame1.joinGame(player5);
        testgame1.setPlayerWhoGetsTurn(player3);
        testgame1.startGame();
        queryLeave.onGuildMessageReceived(event);
    }

    @Test
    public void testLeaveKartu41Game() {

        Game.listGame.put(guild, testgame2);
        testgame2.setCurrentMessageId("123");
        given(event.getAuthor()).willReturn(user2);
        given(event.getMember()).willReturn(member);
        given(member.getUser()).willReturn(user2);
        given(user2.isBot()).willReturn(false);
        given(event.getGuild()).willReturn(guild);
        given(event.getMessage()).willReturn(message);
        given(message.getContentRaw()).willReturn("remee leave");
        given(player2.getUser()).willReturn(user2);
        given(player6.getUser()).willReturn(user1);
        given(user2.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user1.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(event.getChannel()).willReturn(channel1);
        given(channel1.sendMessage(anyString())).willReturn(messageAction);
        given(channel1.deleteMessageById(anyString())).willReturn(deleteMessage);
        testgame2.joinGame(player2);
        testgame2.joinGame(player6);
        given(player2.getNextPlayer()).willReturn(player6);
        given(player6.getNextPlayer()).willReturn(player2);
        testgame2.startGame();
        testgame2.setPlayerWhoGetsTurn(player2);
        queryLeave.onGuildMessageReceived(event);
    }

    @Test
    public void testLeaveIfDoesntHaveAnyPlayer() {
        Game.listGame.put(guild, testgame1);
        given(event.getAuthor()).willReturn(user1);
        given(event.getMember()).willReturn(member);
        given(member.getUser()).willReturn(user1);
        given(user1.isBot()).willReturn(false);
        given(event.getGuild()).willReturn(guild);
        given(event.getMessage()).willReturn(message);
        given(message.getContentRaw()).willReturn("remee leave");
        given(player1.getUser()).willReturn(user1);
        given(event.getChannel()).willReturn(channel1);
        given(channel1.sendMessage(anyString())).willReturn(messageAction);
        queryLeave.onGuildMessageReceived(event);
    }

    @Test
    public void testLeaveGameIfNotTheAuthor() {
        Game.listGame.put(guild, testgame1);
        testgame1.joinGame(player1);
        given(event.getAuthor()).willReturn(user1);
        given(event.getMember()).willReturn(member);
        given(member.getUser()).willReturn(user1);
        given(user1.isBot()).willReturn(false);
        given(event.getGuild()).willReturn(guild);
        given(event.getMessage()).willReturn(message);
        given(message.getContentRaw()).willReturn("remee leave");
        given(player1.getUser()).willReturn(user2);
        given(event.getChannel()).willReturn(channel1);
        given(channel1.sendMessage(anyString())).willReturn(messageAction);
        queryLeave.onGuildMessageReceived(event);
    }

    @Test
    public void testLeaveIfGameHasPlayed() {
        Game.listGame.put(guild, testgame3);
        testgame3.setCurrentMessageId("123");
        testgame3.joinGame(player3);
        testgame3.joinGame(player4);
        testgame3.joinGame(player5);

        given(player3.getUser()).willReturn(user3);
        given(player4.getUser()).willReturn(user4);
        given(player5.getUser()).willReturn(user5);
        given(user3.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user4.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user5.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(player3.getNextPlayer()).willReturn(player4);
        given(player4.getNextPlayer()).willReturn(player5);
        given(player5.getNextPlayer()).willReturn(player3);
        given(player3.getName()).willReturn("Player 3");
        given(player2.getName()).willReturn("Player 4");
        given(player5.getName()).willReturn("Player 5");
        given(event.getChannel()).willReturn(channel1);
        given(channel1.sendMessage(anyString())).willReturn(messageAction);

        given(event.getAuthor()).willReturn(user3);
        given(event.getMember()).willReturn(member);
        given(member.getUser()).willReturn(user3);
        given(user3.isBot()).willReturn(false);
        given(event.getGuild()).willReturn(guild);
        given(event.getMessage()).willReturn(message);
        given(message.getContentRaw()).willReturn("remee leave");

        given(channel1.deleteMessageById(anyString())).willReturn(deleteMessage);
        given(player3.getListCard()).willReturn(cardList);
        given(cardList.size()).willReturn(1);
        given(cardList.remove(0)).will(invocation -> {
            given(cardList.size()).willReturn(0);
            return dummyCard;
        });

        testgame3.startGame();
        queryLeave.onGuildMessageReceived(event);
    }

    @Test
    public void testLeaveIfGameInPreparation() {
        Game.listGame.put(guild, testgame3);
        testgame3.joinGame(player3);
        testgame3.joinGame(player4);
        testgame3.joinGame(player5);
        given(player3.getUser()).willReturn(user3);
        given(player4.getUser()).willReturn(user4);
        given(player5.getUser()).willReturn(user5);
        given(event.getChannel()).willReturn(channel1);
        given(channel1.sendMessage(anyString())).willReturn(messageAction);
        given(event.getAuthor()).willReturn(user3);
        given(event.getMember()).willReturn(member);
        given(member.getUser()).willReturn(user3);
        given(user3.isBot()).willReturn(false);
        given(event.getGuild()).willReturn(guild);
        given(event.getMessage()).willReturn(message);
        given(message.getContentRaw()).willReturn("remee leave");
        queryLeave.onGuildMessageReceived(event);
    }
}


