package id.ac.ui.cs.remeebot.core.game.card;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SpadeTest {
    private Class<?> spadeClass;
    private Spade spade;

    @BeforeEach
    public void setUp() throws Exception {
        spadeClass = Class.forName("id.ac.ui.cs.remeebot.core.game.card.Spade");
        spade = new Spade("test");
    }

    @Test
    public void testGetTypeIsCorrect() {
        assertEquals("Spade", spade.getType());
    }

    @Test
    public void testGetNameAndTypeIsCorrect() {
        assertEquals("test Spade", spade.getNameAndType());
    }

    @Test
    public void testToString() {
        assertEquals("test\u2660", spade.toString());
    }
}
