package id.ac.ui.cs.remeebot.core.game.gameplay;

import id.ac.ui.cs.remeebot.core.game.card.Card;
import id.ac.ui.cs.remeebot.core.game.player.KartuSetanPlayer;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.lang.reflect.Method;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

public class KartuSetanGameTest {
    private Class<?> kartuSetanGameClass;
    private KartuSetanGame kartuSetanGame;

    @Mock
    private MessageReactionAddEvent event;

    @Mock
    private TextChannel channel;

    @Mock
    private KartuSetanPlayer player1;

    @Mock
    private KartuSetanPlayer player2;

    @Mock
    private KartuSetanPlayer player3;

    @Mock
    private User user1;

    @Mock
    private User user2;

    @Mock
    private User user3;

    @Mock
    private RestAction<PrivateChannel> privateChannelRestAction;

    @Mock
    private PrivateChannel privateChannel;

    @Mock
    private MessageAction messageAction;

    @Mock
    private Card dummyCard;

    @BeforeEach
    public void setUp() throws Exception {
        kartuSetanGameClass = Class.forName("id.ac.ui.cs.remeebot.core.game.gameplay.KartuSetanGame");
        MockitoAnnotations.initMocks(this);
        given(event.getTextChannel()).willReturn(channel);
        kartuSetanGame = new KartuSetanGame(event);
        initPlayer();
    }

    public void initPlayer() {
        kartuSetanGame.joinGame(player1);
        kartuSetanGame.joinGame(player2);
        kartuSetanGame.joinGame(player3);
    }

    @Test
    public void testOverrideDeal() throws Exception {
        Method deal = kartuSetanGameClass.getDeclaredMethod("deal", ArrayList.class);

        assertEquals("void", deal.getGenericReturnType().getTypeName());
        assertEquals(1, deal.getParameterCount());
    }

    @Test
    public void testOverrideBroadcast() throws Exception {
        Method broadcast = kartuSetanGameClass.getDeclaredMethod("broadcast");

        assertEquals("void", broadcast.getGenericReturnType().getTypeName());
        assertEquals(0, broadcast.getParameterCount());
    }

    @Test
    public void testOverrideRun() throws Exception {
        Method run = kartuSetanGameClass.getDeclaredMethod("run");

        assertEquals("void", run.getGenericReturnType().getTypeName());
        assertEquals(0, run.getParameterCount());
    }

    @Test
    public void testKartuSetanGameGetName() throws Exception {
        assertEquals("Kartu Setan", kartuSetanGame.getGameName());
    }

    @Test
    public void testKartuSetanGameGetChannel() throws Exception {
        assertEquals(channel, kartuSetanGame.getChannel());
    }

    @Test
    public void testKartuSetanGameGetPlayers() throws Exception {
        assertEquals(3, kartuSetanGame.getPlayers().size());
    }

    @Test
    public void testStartKartuSetanGame() throws Exception {
        given(player1.getUser()).willReturn(user1);
        given(player2.getUser()).willReturn(user2);
        given(player3.getUser()).willReturn(user3);
        given(user1.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user2.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user3.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(player1.getNextPlayer()).willReturn(player2);
        given(player2.getNextPlayer()).willReturn(player3);
        given(player3.getNextPlayer()).willReturn(player1);
        given(player1.getName()).willReturn("Player 1");
        given(player2.getName()).willReturn("Player 2");
        given(player3.getName()).willReturn("Player 3");
        given(channel.sendMessage(anyString())).willReturn(messageAction);
        assertTrue(kartuSetanGame.startGame());
    }

    @Test
    public void testBroadcastMessage() throws Exception {
        kartuSetanGame.setPlayerWhoGetsTurn(player1);
        given(player1.getNextPlayer()).willReturn(player2);
        given(player1.getUser()).willReturn(user1);
        given(player2.getUser()).willReturn(user2);
        given(user1.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user2.openPrivateChannel()).willReturn(privateChannelRestAction);
        kartuSetanGame.broadcast();
        assertTrue(true);
    }

    @Test
    public void testRunKartuSetanGame() throws Exception {
        kartuSetanGame.setPlayerWhoGetsTurn(player1);
        given(player1.getName()).willReturn("Player 1");
        given(player1.getNextPlayer()).willReturn(player2);
        given(player2.getName()).willReturn("Player 2");
        given(channel.sendMessage(anyString())).willReturn(messageAction);
        kartuSetanGame.run();
        assertTrue(true);
    }
}