package id.ac.ui.cs.remeebot.core.game.gameplay;

import id.ac.ui.cs.remeebot.core.game.card.Card;
import id.ac.ui.cs.remeebot.core.game.player.Kartu41Player;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Stack;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

public class Kartu41GameTest {
    private Class<?> kartu41GameClass;
    private Kartu41Game kartu41Game;

    @Mock
    private MessageReactionAddEvent event;

    @Mock
    private TextChannel channel;

    @Mock
    private Kartu41Player player1;

    @Mock
    private Kartu41Player player2;

    @Mock
    private Kartu41Player player3;

    @Mock
    private Kartu41Player player4;

    @Mock
    private User user1;

    @Mock
    private User user2;

    @Mock
    private User user3;

    @Mock
    private User user4;

    @Mock
    private PrivateChannel privateChannel;

    @Mock
    private MessageAction messageAction;

    @Mock
    private Card dummyCard;

    @Mock
    private RestAction<PrivateChannel> privateChannelRestAction;

    @Mock
    private Stack<Card> cardStack;

    @Mock
    private Guild guild;

    @BeforeEach
    public void setUp() throws Exception {
        kartu41GameClass = Class.forName("id.ac.ui.cs.remeebot.core.game.gameplay.Kartu41Game");
        MockitoAnnotations.initMocks(this);
        given(event.getTextChannel()).willReturn(channel);
        kartu41Game = new Kartu41Game(event);
        initPlayer();
    }

    public void initPlayer() {
        kartu41Game.joinGame(player1);
        kartu41Game.joinGame(player2);
        kartu41Game.joinGame(player3);
        kartu41Game.joinGame(player4);
    }

    @Test
    public void testOverrideDeal() throws Exception {
        Method deal = kartu41GameClass.getDeclaredMethod("deal", ArrayList.class);

        assertEquals("void", deal.getGenericReturnType().getTypeName());
        assertEquals(1, deal.getParameterCount());
    }

    @Test
    public void testOverrideBroadcast() throws Exception {
        Method broadcast = kartu41GameClass.getDeclaredMethod("broadcast");

        assertEquals("void", broadcast.getGenericReturnType().getTypeName());
        assertEquals(0, broadcast.getParameterCount());
    }


    @Test
    public void testOverrideRun() throws Exception {
        Method run = kartu41GameClass.getDeclaredMethod("run");

        assertEquals("void", run.getGenericReturnType().getTypeName());
        assertEquals(0, run.getParameterCount());
    }

    @Test
    public void testKartu41GameGetName() throws Exception {
        assertEquals("Kartu 41", kartu41Game.getGameName());
    }

    @Test
    public void testKartu41GameGetChannel() throws Exception {
        assertEquals(channel, kartu41Game.getChannel());
    }

    @Test
    public void testKartu41GameGetPlayers() throws Exception {
        assertEquals(4, kartu41Game.getPlayers().size());
    }

    @Test
    public void testStartKartu41Game() throws Exception {
        given(player1.getUser()).willReturn(user1);
        given(player2.getUser()).willReturn(user2);
        given(player3.getUser()).willReturn(user3);
        given(player4.getUser()).willReturn(user4);
        given(user1.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user2.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user3.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user4.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(player1.getNextPlayer()).willReturn(player2);
        given(player2.getNextPlayer()).willReturn(player3);
        given(player3.getNextPlayer()).willReturn(player4);
        given(player4.getNextPlayer()).willReturn(player1);
        given(player1.getName()).willReturn("Player 1");
        given(player2.getName()).willReturn("Player 2");
        given(player3.getName()).willReturn("Player 3");
        given(player4.getName()).willReturn("Player 4");
        given(channel.sendMessage(anyString())).willReturn(messageAction);
        assertTrue(kartu41Game.startGame());
    }

    @Test
    public void testBroadcastMessage() throws Exception {
        kartu41Game.setPlayerWhoGetsTurn(player1);
        given(player1.getNextPlayer()).willReturn(player2);
        given(player1.getUser()).willReturn(user1);
        given(player2.getUser()).willReturn(user2);
        given(user1.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user2.openPrivateChannel()).willReturn(privateChannelRestAction);
        kartu41Game.broadcast();
        assertTrue(true);
    }

    @Test
    public void testRunKartu41GameFirstPlayer() throws Exception {
        kartu41Game.setPlayerWhoGetsTurn(player1);
        given(player1.getName()).willReturn("Player 1");
        given(player1.getNextPlayer()).willReturn(player2);
        given(player2.getName()).willReturn("Player 2");
        given(channel.sendMessage(anyString())).willReturn(messageAction);
        kartu41Game.run();
        assertTrue(true);
    }

    @Test
    public void testRunKartu41GameNotFirstPlayer() throws Exception {
        given(event.getChannel()).willReturn(channel);
        given(channel.getGuild()).willReturn(guild);
        Game.listGame.put(guild, kartu41Game);
        kartu41Game.setPlayerWhoGetsTurn(player1);
        given(player1.getUser()).willReturn(user1);
        given(player2.getUser()).willReturn(user2);
        given(player3.getUser()).willReturn(user3);
        given(player4.getUser()).willReturn(user4);
        given(user1.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user2.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user3.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user4.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(player1.getNextPlayer()).willReturn(player2);
        given(player2.getNextPlayer()).willReturn(player3);
        given(player3.getNextPlayer()).willReturn(player4);
        given(player4.getNextPlayer()).willReturn(player1);
        given(player1.getName()).willReturn("Player 1");
        given(player2.getName()).willReturn("Player 2");
        given(player3.getName()).willReturn("Player 3");
        given(player4.getName()).willReturn("Player 4");
        given(channel.sendMessage(anyString())).willReturn(messageAction);
        given(player1.getDumpedCard()).willReturn(cardStack);
        given(cardStack.peek()).willReturn(dummyCard);
        kartu41Game.run();
        kartu41Game.deckPop("Ambil dari Deck");
        kartu41Game.run();
        assertTrue(true);
    }

    @Test
    public void testDeckPopKartu41GameFromDumped() throws Exception {
        kartu41Game.setPlayerWhoGetsTurn(player1);
        given(player1.getUser()).willReturn(user1);
        given(player2.getUser()).willReturn(user2);
        given(user1.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user2.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(player1.getName()).willReturn("Player 1");
        given(player1.getNextPlayer()).willReturn(player2);
        given(player2.getName()).willReturn("Player 2");
        given(channel.sendMessage(anyString())).willReturn(messageAction);
        given(player1.getDumpedCard()).willReturn(cardStack);
        kartu41Game.deckPop("Ambil dari Buangan");
        assertTrue(true);
    }

    @Test
    public void testWinGameKartu41() throws Exception {
        given(event.getGuild()).willReturn(guild);
        kartu41Game.setPlayerWhoGetsTurn(player1);
        given(channel.sendMessage(anyString())).willReturn(messageAction);
        given(player1.getUser()).willReturn(user1);
        given(user1.openPrivateChannel()).willReturn(privateChannelRestAction);
        Kartu41Player currentPlayer = (Kartu41Player) kartu41Game.getPlayerWhoGetsTurn();
        given(currentPlayer.check41()).willReturn(true);
        kartu41Game.winGame();
    }

}
