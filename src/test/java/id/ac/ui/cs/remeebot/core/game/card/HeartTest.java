package id.ac.ui.cs.remeebot.core.game.card;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HeartTest {
    private Class<?> heartClass;
    private Heart heart;

    @BeforeEach
    public void setUp() throws Exception {
        heartClass = Class.forName("id.ac.ui.cs.remeebot.core.game.card.Heart");
        heart = new Heart("test");
    }

    @Test
    public void testGetTypeIsCorrect() {
        assertEquals("Heart", heart.getType());
    }

    @Test
    public void testGetNameAndTypeIsCorrect() {
        assertEquals("test Heart", heart.getNameAndType());
    }

    @Test
    public void testToString() {
        assertEquals("test\u2665", heart.toString());
    }
}
