package id.ac.ui.cs.remeebot.core.game.card;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ClubTest {
    private Class<?> clubClass;
    private Club club;

    @BeforeEach
    public void setUp() throws Exception {
        clubClass = Class.forName("id.ac.ui.cs.remeebot.core.game.card.Club");
        club = new Club("test");
    }

    @Test
    public void testGetTypeIsCorrect() {
        assertEquals("Club", club.getType());
    }

    @Test
    public void testGetNameAndTypeIsCorrect() {
        assertEquals("test Club", club.getNameAndType());
    }

    @Test
    public void testToString() {
        assertEquals("test\u2663", club.toString());
    }
}
