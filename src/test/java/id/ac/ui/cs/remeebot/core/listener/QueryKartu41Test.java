package id.ac.ui.cs.remeebot.core.listener;

import id.ac.ui.cs.remeebot.core.game.card.Card;
import id.ac.ui.cs.remeebot.core.game.gameplay.Game;
import id.ac.ui.cs.remeebot.core.game.gameplay.Kartu41Game;
import id.ac.ui.cs.remeebot.core.game.gameplay.KartuSetanGame;
import id.ac.ui.cs.remeebot.core.game.player.Kartu41Player;
import id.ac.ui.cs.remeebot.core.game.player.Player;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.requests.restaction.AuditableRestAction;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Map;
import java.util.Stack;

import static id.ac.ui.cs.remeebot.core.game.ConstantEmote.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

public class QueryKartu41Test {
    private QueryKartu41 queryKartu41;
    private Game game;

    @Mock
    private MessageReactionAddEvent event;

    @Mock
    private GuildMessageReactionAddEvent eventGuild;

    @Mock
    private Guild guild;

    @Mock
    private TextChannel channel;

    @Mock
    private Map<Guild, Game> listGame = Game.listGame;

    @Mock
    private MessageReaction.ReactionEmote reactionEmote;

    @Mock
    private User user1;

    @Mock
    private User user2;

    @Mock
    private Kartu41Player player1;

    @Mock
    private Kartu41Player player2;

    @Mock
    private RestAction<PrivateChannel> privateChannelRestAction;

    @Mock
    private MessageAction messageAction;

    @Mock
    private Stack<Card> cardStack;

    @Mock
    AuditableRestAction deleteMessage;


    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        setUpGameAndPlayer();
        queryKartu41 = new QueryKartu41();
    }

    public void setUpGameAndPlayer() {
        given(event.getTextChannel()).willReturn(channel);
        game = new Kartu41Game(event);
        Game.listGame.put(guild, game);

        game.joinGame(player1);
        game.joinGame(player2);
    }

    @Test
    public void testThrownCard() throws Exception {
        game.setPlayerWhoGetsTurn(player1);
        given(eventGuild.getGuild()).willReturn(guild);
        given(listGame.containsKey(guild)).willReturn(true);
        given(listGame.get(guild)).willReturn(game);
        given(player1.getNextPlayer()).willReturn(player2);
        given(player1.getUser()).willReturn(user1);
        given(eventGuild.getUser()).willReturn(user1);
        given(eventGuild.getReactionEmote()).willReturn(reactionEmote);
        given(reactionEmote.getAsCodepoints()).willReturn(ICON_A);
        given(player1.getNextPlayer()).willReturn(player2);
        given(player2.getNextPlayer()).willReturn(player1);
        given(player2.getUser()).willReturn(user2);
        given(user1.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user2.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(player1.getName()).willReturn("Player 1");
        given(player2.getName()).willReturn("Player 2");
        given(channel.sendMessage(anyString())).willReturn(messageAction);
        given(channel.deleteMessageById(event.getMessageId())).willReturn(deleteMessage);
        queryKartu41.onGuildMessageReactionAdd(eventGuild);
    }
    @Test
    public void testDrawCardFromDeck() throws Exception {
        game.setPlayerWhoGetsTurn(player1);
        given(eventGuild.getGuild()).willReturn(guild);
        given(listGame.containsKey(guild)).willReturn(true);
        given(listGame.get(guild)).willReturn(game);
        given(player1.getNextPlayer()).willReturn(player2);
        given(player1.getUser()).willReturn(user1);
        given(eventGuild.getUser()).willReturn(user1);
        given(eventGuild.getReactionEmote()).willReturn(reactionEmote);
        given(reactionEmote.getAsCodepoints()).willReturn(ICON_HANDS_LEFT);
        given(player1.getNextPlayer()).willReturn(player2);
        given(player2.getNextPlayer()).willReturn(player1);
        given(player2.getUser()).willReturn(user2);
        given(user1.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user2.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(player1.getName()).willReturn("Player 1");
        given(player2.getName()).willReturn("Player 2");
        given(channel.sendMessage(anyString())).willReturn(messageAction);
        given(channel.deleteMessageById(event.getMessageId())).willReturn(deleteMessage);
        queryKartu41.onGuildMessageReactionAdd(eventGuild);
    }
    @Test
    public void testDrawCardFromDumped() throws Exception {
        game.setPlayerWhoGetsTurn(player1);
        given(eventGuild.getGuild()).willReturn(guild);
        given(listGame.containsKey(guild)).willReturn(true);
        given(listGame.get(guild)).willReturn(game);
        given(player1.getNextPlayer()).willReturn(player2);
        given(player1.getUser()).willReturn(user1);
        given(eventGuild.getUser()).willReturn(user1);
        given(eventGuild.getReactionEmote()).willReturn(reactionEmote);
        given(reactionEmote.getAsCodepoints()).willReturn(ICON_HANDS_RIGHT);
        given(player1.getNextPlayer()).willReturn(player2);
        given(player2.getNextPlayer()).willReturn(player1);
        given(player2.getUser()).willReturn(user2);
        given(user1.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(user2.openPrivateChannel()).willReturn(privateChannelRestAction);
        given(player1.getName()).willReturn("Player 1");
        given(player2.getName()).willReturn("Player 2");
        given(channel.sendMessage(anyString())).willReturn(messageAction);
        given(channel.deleteMessageById(event.getMessageId())).willReturn(deleteMessage);
        given(player1.getDumpedCard()).willReturn(cardStack);
        queryKartu41.onGuildMessageReactionAdd(eventGuild);
    }
}
