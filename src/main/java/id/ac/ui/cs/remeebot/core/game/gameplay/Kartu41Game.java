package id.ac.ui.cs.remeebot.core.game.gameplay;

import id.ac.ui.cs.remeebot.core.game.card.Card;
import id.ac.ui.cs.remeebot.core.game.player.Kartu41Player;
import id.ac.ui.cs.remeebot.core.game.player.Player;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;

import java.util.*;

import static id.ac.ui.cs.remeebot.core.game.ConstantEmote.*;

public class Kartu41Game extends Game {
    private final int NUMBER_OF_CARD_PER_PLAYER = 4;
    private boolean firstTurn = true;
    private final List<String> reactions = new ArrayList<>(
            Arrays.asList(
                    ICON_A, ICON_B, ICON_C, ICON_D, ICON_E
            ));

    public Kartu41Game(MessageReactionAddEvent newGameEvent) {
        super(newGameEvent);
        this.gameName = "Kartu 41";
        maxPlayer = 4;
        minPlayer = 2;
    }


    @Override
    public void deal(ArrayList<Player> players) {
        Stack<Card> cardList = cardRepository.getCardList();
        Collections.shuffle(cardList);

        for (Player player : players) {
            for (int orderOfCard = 1; orderOfCard <= NUMBER_OF_CARD_PER_PLAYER; orderOfCard++) {
                Card newCard = cardList.pop();
                player.getCard(newCard);
            }
        }
        rafflePlayers();
        Player firstPlayer = getPlayerWhoGetsTurn();
        Card startingCard = cardList.pop();
        firstPlayer.getCard(startingCard);

        for (Player player : this.getPlayers()) {
            String cardsOfPlayer = player.getDist();
            sendToPrivateChannel(player, cardsOfPlayer);
        }

        String currentPlayerName = firstPlayer.getName();
        TextChannel channel = this.getChannel();

        channel.sendMessage(String.format("Silahkan %s membuang kartumu", currentPlayerName)).queue(
                message -> {
                    this.setCurrentMessageId(message.getId());
                    for (int order = 0; order <= 4; order++) {
                        String reaction = reactions.get(order);
                        message.addReaction(reaction).queue();
                    }
                });
    }


    @Override
    public void broadcast() {
        Player playerWhoGetsTurn = this.getPlayerWhoGetsTurn();
        String playerDist = playerWhoGetsTurn.getDist();
        sendToPrivateChannel(playerWhoGetsTurn, playerDist);
    }

    @Override
    public void run() {
        Guild guild = this.getChannel().getGuild();
        if (!Game.listGame.containsKey(guild)) {
            return;
        }
        if (!firstTurn) {
            Kartu41Player currentPlayer = (Kartu41Player) this.getPlayerWhoGetsTurn();
            String currentPlayerName = currentPlayer.getName();
            Stack<Card> currentPlayerDumpedCard = currentPlayer.getDumpedCard();
            TextChannel channel = this.getChannel();
            if (!currentPlayerDumpedCard.isEmpty()) {
                String topInDumpedCard = currentPlayerDumpedCard.peek().toString();
                channel.sendMessage(String.format("```Silahkan %s memilih ingin mengambil kartu di deck atau dari buangan\n" +
                        "👈 Ambil dari Deck\n" +
                        "👉 Ambil dari Buangan (%s)```", currentPlayerName, topInDumpedCard)).queue(
                        message -> {
                            this.setCurrentMessageId(message.getId());
                            message.addReaction("👈").queue();
                            message.addReaction("👉").queue();
                        }
                );
            } else {
                channel.sendMessage(String.format("```Silahkan %s memilih ingin mengambil kartu di deck atau dari buangan\n" +
                        "👈 Ambil dari Deck```", currentPlayerName)).queue(
                        message -> {
                            this.setCurrentMessageId(message.getId());
                            message.addReaction("👈").queue();
                        }
                );
            }


        } else {
            this.firstTurn = false;
        }
    }

    public void deckPop(String type) {

        Kartu41Player currentPlayer = (Kartu41Player) this.getPlayerWhoGetsTurn();
        String currentPlayerName = currentPlayer.getName();
        Card cardDrawn = null;

        if (type.equals("Ambil dari Deck")) {
            Stack<Card> deck = cardRepository.getCardList();
            cardDrawn = deck.pop();

        } else if (type.equals("Ambil dari Buangan")) {
            Stack<Card> currentPlayerDumpedCard = currentPlayer.getDumpedCard();
            boolean dumpedCardisNotEmpty = !currentPlayerDumpedCard.isEmpty();

            if (dumpedCardisNotEmpty) {
                cardDrawn = currentPlayerDumpedCard.pop();
            }
        }

        if (cardDrawn != null) {
            currentPlayer.addCard(cardDrawn);
        }

        TextChannel channel = this.getChannel();
        broadcast();

        channel.sendMessage(String.format("```%s memilih untuk %s\n```", currentPlayerName, type)).queue();
        channel.sendMessage(String.format("```Silahkan %s untuk segera membuang kartumu```", currentPlayerName)).queue(
                message -> {
                    this.setCurrentMessageId(message.getId());
                    for (int order = 0; order <= 4; order++) {
                        String reaction = reactions.get(order);
                        message.addReaction(reaction).queue();
                    }
                });
    }

    @Override
    public void winGame() {
        Kartu41Player currentPlayer = (Kartu41Player) this.getPlayerWhoGetsTurn();
        Stack<Card> deck = cardRepository.getCardList();
        ArrayList<Player> listPlayer = this.getPlayers();
        if (currentPlayer.check41()) {
            this.getChannel().sendMessage("Selamat " + currentPlayer.getName() + ", 41!!").queue();
            finishGame41();
        } else if (deck.isEmpty() || listPlayer.size() == 1) {
            finishGame41();
        }
    }

    private void finishGame41() {
        for (Player player : this.getPlayers()) {
            Kartu41Player player41 = (Kartu41Player) player;
            player41.countRest();
            addLeaderboard(player41);
        }
        Collections.sort(leaderboard);

        Guild guild = this.getChannel().getGuild();
        Game.listGame.remove(guild);
        TextChannel channel = this.getChannel();
        channel.sendMessage(showLeaderboard()).queue();
    }

    public String showLeaderboard() {
        int[] lastPointsAndRank = new int[2];
        int rank;
        String result = TROPHY + " LEADERBOARD " + TROPHY + "\n\n";
        for (int currentIndex = 1; currentIndex <= leaderboard.size(); currentIndex++) {
            String player = leaderboard.get(currentIndex - 1).getName();
            int finalPoints = ((Kartu41Player) leaderboard.get(currentIndex - 1)).getFinalPoints();
            if (finalPoints == lastPointsAndRank[0]) {
                rank = lastPointsAndRank[1];
            } else {
                rank = currentIndex;
                lastPointsAndRank[0] = finalPoints;
                lastPointsAndRank[1] = rank;
            }
            switch (rank) {
                case (1):
                    result += String.format("%s %s (%d pts)\n", FIRST_PLACE, player, finalPoints);
                    break;
                case (2):
                    result += String.format("%s %s (%d pts)\n", SECOND_PLACE, player, finalPoints);
                    break;
                case (3):
                    result += String.format("%s %s (%d pts)\n", THIRD_PLACE, player, finalPoints);
                    break;
                default:
                    result += String.format("%d. %s (%d pts)\n", rank, player, finalPoints);
            }
        }
        return result;
    }

    @Override
    public void leaveInGame(Player player) {
        Stack<Card> currentDeck = cardRepository.getCardList();
        List<Card> listCardPlayer = player.getListCard();

        Player currentPlayer = this.getPlayerWhoGetsTurn();

        for (Card card : listCardPlayer) {
            currentDeck.push(card);
        }
        Collections.shuffle(currentDeck);

        Player nextPlayer = player.getNextPlayer();
        Player prevPlayer = player;
        System.out.println(prevPlayer);
        while (player != prevPlayer.getNextPlayer()) {
            prevPlayer = prevPlayer.getNextPlayer();
        }

        ArrayList<Player> listPlayer = this.getPlayers();

        prevPlayer.setNextPlayer(nextPlayer);
        listPlayer.remove(player);

        TextChannel channel = this.getChannel();
        String messageId = this.getCurrentMessageId();
        channel.deleteMessageById(messageId).queue();
        if (this.getPlayerWhoGetsTurn() == player) this.setPlayerWhoGetsTurn(nextPlayer);
        if (this.players.size() > 1) this.run();

    }
}
