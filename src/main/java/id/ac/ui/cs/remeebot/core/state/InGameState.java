package id.ac.ui.cs.remeebot.core.state;

import id.ac.ui.cs.remeebot.core.game.gameplay.Game;
import id.ac.ui.cs.remeebot.core.game.player.Player;

public class InGameState extends State {
    public InGameState(Game game) {
        super(game);
    }

    @Override
    public boolean join(Player player) {
        return false;
    }

    @Override
    public boolean start() {
        return false;
    }

    @Override
    public boolean leave(Player player){
        game.leaveInGame(player);
        game.winGame();
        return true;
    }
}
