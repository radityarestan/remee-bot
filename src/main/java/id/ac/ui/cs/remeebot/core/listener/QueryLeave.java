package id.ac.ui.cs.remeebot.core.listener;

import id.ac.ui.cs.remeebot.core.game.gameplay.Game;
import id.ac.ui.cs.remeebot.core.game.player.Player;
import id.ac.ui.cs.remeebot.core.state.InGameState;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.SortedSet;

public class QueryLeave extends ListenerAdapter {

    @Override
    public void onGuildMessageReceived(@NotNull GuildMessageReceivedEvent event) {
        User author = event.getAuthor();


        Member member = event.getMember();
        User memberUser = member.getUser();
        boolean isNotBot = !memberUser.isBot();

        Guild guild = event.getGuild();
        boolean isActiveGame = Game.listGame.containsKey(guild);

        Message incomingMessage = event.getMessage();
        String rawIncomingMessage = incomingMessage.getContentRaw();
        boolean isEqualLeave = rawIncomingMessage.equals("remee leave");

        if (isNotBot && isActiveGame && isEqualLeave) {
            Game game = Game.listGame.get(guild);
            ArrayList<Player> players = game.getPlayers();
            Player player = getAuthorPlayer(players, author);
            MessageChannel messageChannel = event.getChannel();

            if (player == null || !players.contains(player)) return;
            announceExit(messageChannel, author, game);
            game.leave(player);
        }
    }

    private Player getAuthorPlayer(ArrayList<Player> players, User author) {
        for (Player player : players) {
            User playerUser = player.getUser();
            boolean isSameAuthor = playerUser.equals(author);
            if (isSameAuthor) {
                return player;
            }
        }
        return null;
    }

    private void announceExit(MessageChannel messageChannel, User author, Game game) {
        String gameName = game.getGameName();
        boolean isKartuSetan = gameName.equals("Kartu Setan");
        boolean isKartu41 = gameName.equals("Kartu 41");
        String authorName = author.getName();

        MessageAction messageAction;
        if (isKartuSetan) {
            messageAction = messageChannel.sendMessage(authorName + " exited the game");
            messageAction.queue();
        } else if (isKartu41) {
            messageAction = messageChannel.sendMessage(authorName + " exited the game");
            messageAction.queue();
        }
    }
}
