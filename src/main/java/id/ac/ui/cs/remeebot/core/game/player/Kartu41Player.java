package id.ac.ui.cs.remeebot.core.game.player;

import id.ac.ui.cs.remeebot.core.game.card.Card;
import net.dv8tion.jda.api.entities.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class Kartu41Player extends Player {
    private int spadeCount = 0;
    private int heartCount = 0;
    private int diamondCount = 0;
    private int clubCount = 0;
    private int finalPoints;
    private final List<String> alfabetDist = new ArrayList<>(
            Arrays.asList(
                    "A", "B", "C", "D", "E"
            ));
    protected Stack<Card> dumpedCard;

    public Kartu41Player(User user) {
        super(user);
        dumpedCard = new Stack<>();
    }

    public Stack<Card> getDumpedCard() {
        return this.dumpedCard;
    }

    public void setFinalPoints(int finalPoints) {
        this.finalPoints = finalPoints;
    }

    public int getFinalPoints() {
        return this.finalPoints;
    }

    @Override
    public Card run(String card) {
        Kartu41Player nextPlayer = (Kartu41Player) this.getNextPlayer();
        String responseInHexa = card.substring(2);
        int orderOfCard = Integer.parseInt(responseInHexa, 16) % 127462;

        Card thrownCard = this.removeCard(orderOfCard);
        Stack<Card> nextPlayerDumpedCard = nextPlayer.getDumpedCard();
        nextPlayerDumpedCard.push(thrownCard);
        return thrownCard;
    }

    public String getDist() {
        String hasil = "This is Your Card\n";
        for (Card card : listCard) {
            hasil += card + " ";
        }
        hasil += "\n";
        for (String alfabet : alfabetDist) {
            hasil += alfabet + "  ";
        }
        return "```" + hasil + "```";
    }

    public boolean check41() {
        this.countEach();
        if (spadeCount == 41 || heartCount == 41 || diamondCount == 41 || clubCount == 41) {
            this.finalPoints = 41;
            return true;
        }
        return false;
    }

    public void countRest() {
        this.countEach();
        int currentMost;

        currentMost = spadeCount - heartCount - clubCount - diamondCount;
        this.finalPoints = Math.max(currentMost, this.finalPoints);
        currentMost = heartCount - spadeCount - clubCount - diamondCount;
        this.finalPoints = Math.max(currentMost, this.finalPoints);
        currentMost = clubCount - spadeCount - heartCount - diamondCount;
        this.finalPoints = Math.max(currentMost, this.finalPoints);
        currentMost = diamondCount - spadeCount - heartCount - clubCount;
        this.finalPoints = Math.max(currentMost, this.finalPoints);
    }

    public void countEach() {
        spadeCount = 0;
        heartCount = 0;
        diamondCount = 0;
        clubCount = 0;
        for (Card card : listCard) {
            String cardType = card.getType();
            int cardInt = card.getValue();
            switch (cardType) {
                case "Spade":
                    spadeCount += cardInt;
                    break;
                case "Heart":
                    heartCount += cardInt;
                    break;
                case "Club":
                    clubCount += cardInt;
                    break;
                case "Diamond":
                    diamondCount += cardInt;
                    break;
            }
        }
    }

    @Override
    public int compareTo(Player other) {
        Kartu41Player that = (Kartu41Player) other;
        return Integer.compare(that.finalPoints, this.finalPoints);
    }

}
