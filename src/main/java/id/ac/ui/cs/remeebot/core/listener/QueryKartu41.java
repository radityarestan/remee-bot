package id.ac.ui.cs.remeebot.core.listener;

import id.ac.ui.cs.remeebot.core.game.ConstantEmote;
import id.ac.ui.cs.remeebot.core.game.card.Card;
import id.ac.ui.cs.remeebot.core.game.gameplay.Game;
import id.ac.ui.cs.remeebot.core.game.gameplay.Kartu41Game;
import id.ac.ui.cs.remeebot.core.game.player.Player;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class QueryKartu41 extends ListenerAdapter {
    @Override
    public void onGuildMessageReactionAdd(GuildMessageReactionAddEvent event) {
        boolean isGameExist = Game.listGame.containsKey(event.getGuild());
        boolean isPlayerExist = false;
        Game currentGame = null;


        if (isGameExist) {
            currentGame = Game.listGame.get(event.getGuild());
            isPlayerExist = currentGame.getPlayerWhoGetsTurn() != null ? true : false;
        }

        if (currentGame instanceof Kartu41Game && isPlayerExist) {

            Kartu41Game current41Game = (Kartu41Game) currentGame;

            Player currentPlayer = current41Game.getPlayerWhoGetsTurn();
            String currentPlayerName = currentPlayer.getName();
            Player nextPlayer = currentPlayer.getNextPlayer();
            String nextPlayerName = nextPlayer.getName();
            User userOfCurrentPlayer = currentPlayer.getUser();
            boolean isUserWhoGetsTurn = event.getUser().equals(userOfCurrentPlayer);

            if (isUserWhoGetsTurn) {
                currentGame.setCurrentMessageId(event.getMessageId());
                String responseCurrentUser = event.getReactionEmote().getAsCodepoints();
                boolean isHandsLeftEmote = responseCurrentUser.equalsIgnoreCase(ConstantEmote.ICON_HANDS_LEFT);
                boolean isHandsRightEmote = responseCurrentUser.equalsIgnoreCase(ConstantEmote.ICON_HANDS_RIGHT);
                boolean isIconA = responseCurrentUser.equalsIgnoreCase(ConstantEmote.ICON_A);
                boolean isIconB = responseCurrentUser.equalsIgnoreCase(ConstantEmote.ICON_B);
                boolean isIconC = responseCurrentUser.equalsIgnoreCase(ConstantEmote.ICON_C);
                boolean isIconD = responseCurrentUser.equalsIgnoreCase(ConstantEmote.ICON_D);
                boolean isIconE = responseCurrentUser.equalsIgnoreCase(ConstantEmote.ICON_E);

                TextChannel channel = currentGame.getChannel();
                if (isHandsLeftEmote || isHandsRightEmote) {
                    if (isHandsLeftEmote) {
                        current41Game.deckPop("Ambil dari Deck");
                    } else if (isHandsRightEmote) {
                        current41Game.deckPop("Ambil dari Buangan");
                    }
                    String messageId = event.getMessageId();
                    channel.deleteMessageById(messageId).queue();
                } else if(isIconA || isIconB || isIconC || isIconD || isIconE) {
                    Card thrownCard = currentPlayer.run(responseCurrentUser);
                    channel.sendMessage(
                            String.format("```%s memilih untuk membuang %s```", currentPlayerName, thrownCard)).queue(
                    );
                    currentGame.winGame();
                    currentGame.broadcast();
                    currentGame.setPlayerWhoGetsTurn(nextPlayer);
                    currentGame.run();
                    String messageId = event.getMessageId();
                    channel.deleteMessageById(messageId).queue();
                }
            }
        }
    }
}
