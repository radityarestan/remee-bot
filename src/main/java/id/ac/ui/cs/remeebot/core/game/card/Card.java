package id.ac.ui.cs.remeebot.core.game.card;

public class Card {
    private String name;
    protected String type;

    public Card(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public String getNameAndType() {
        return this.name + " " + getType();
    }

    public String getType() {
        return type;
    }

    public int getValue() {
        switch (name) {
            case "J":
            case "Q":
            case "K":
                return 10;
            case "A":
                return 11;
            default:
                return Integer.parseInt(name);
        }
    }
}
