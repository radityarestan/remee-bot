package id.ac.ui.cs.remeebot.core.listener;

import id.ac.ui.cs.remeebot.core.game.gameplay.Game;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.requests.restaction.MessageAction;

public class QueryBye extends ListenerAdapter {
    private final String details = "Thank you for playing with Remee";

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        Guild guild = event.getGuild();
        boolean isActiveGame = Game.listGame.containsKey(guild);

        TextChannel textChannel = event.getChannel();
        Message incomingMessage = event.getMessage();
        String rawIncomingMessage = incomingMessage.getContentRaw();
        boolean isEqualBye = rawIncomingMessage.equals("remee bye");
        if (isEqualBye) {
            if (isActiveGame) Game.listGame.remove(guild);
            MessageAction toSend = textChannel.sendMessage(details);
            RestAction<Void> toLeave = guild.leave();
            toSend.queue();
            toLeave.queue();
        }
    }

}