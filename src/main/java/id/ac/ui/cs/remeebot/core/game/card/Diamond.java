package id.ac.ui.cs.remeebot.core.game.card;

import static id.ac.ui.cs.remeebot.core.game.ConstantEmote.ICON_DIAMOND;

public class Diamond extends Card {
    public Diamond(String name) {
        super(name);
        this.type = "Diamond";
    }

    @Override
    public String toString() {
        return this.getName() + ICON_DIAMOND;
    }
}
