package id.ac.ui.cs.remeebot.core.game.card;

import static id.ac.ui.cs.remeebot.core.game.ConstantEmote.ICON_HEART;

public class Heart extends Card {
    public Heart(String name) {
        super(name);
        this.type = "Heart";
    }

    @Override
    public String toString() {
        return this.getName() + ICON_HEART;
    }
}
