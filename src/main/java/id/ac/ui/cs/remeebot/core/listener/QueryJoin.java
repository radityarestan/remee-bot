package id.ac.ui.cs.remeebot.core.listener;

import id.ac.ui.cs.remeebot.core.game.ConstantEmote;
import id.ac.ui.cs.remeebot.core.game.gameplay.Game;
import id.ac.ui.cs.remeebot.core.game.player.Kartu41Player;
import id.ac.ui.cs.remeebot.core.game.player.KartuSetanPlayer;
import id.ac.ui.cs.remeebot.core.game.player.Player;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.ArrayList;

public class QueryJoin extends ListenerAdapter {

    @Override
    public void onMessageReactionAdd(MessageReactionAddEvent event) {
        Member member = event.getMember();
        User memberUser = member.getUser();
        boolean isNotBot = !memberUser.isBot();

        Guild guild = event.getGuild();
        boolean isActiveGame = Game.listGame.containsKey(guild);

        String codepoints = event.getReactionEmote().getAsCodepoints();
        boolean isHandsUpEmote = codepoints.equalsIgnoreCase(ConstantEmote.ICON_HANDS_UP);


        if (isNotBot && isActiveGame && isHandsUpEmote) {
            Game game = Game.listGame.get(guild);
            ArrayList<Player> players = game.getPlayers();
            boolean isExist = false;

            User user = event.getUser();
            for (Player player1 : players) {
                User currentPlayer = player1.getUser();
                boolean isSameUser = user.equals(currentPlayer);

                if (isSameUser) {
                    isExist = true;
                    break;
                }
            }

            Player player = null;

            String gameName = game.getGameName();
            boolean isKartuSetan = gameName.equals("Kartu Setan");
            boolean is41 = gameName.equals("Kartu 41");
            if (!isExist) {
                if (isKartuSetan) {
                    player = new KartuSetanPlayer(user, game);
                } else if (is41) {
                    player = new Kartu41Player(user);
                }

                boolean isJoin = game.joinGame(player);
                if (isJoin) {
                    MessageChannel messageChannel = event.getChannel();
                    String nameUser = user.getName();
                    String joined = " joined the game";
                    String joinedGameText = nameUser.concat(joined);
                    messageChannel.sendMessage(joinedGameText).queue();
                }
            }
        }
    }
}

