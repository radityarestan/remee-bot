package id.ac.ui.cs.remeebot.core.game;

public class ConstantEmote {
    public static final String ICON_SPADE = "\u2660";
    public static final String ICON_HEART = "\u2665";
    public static final String ICON_DIAMOND = "\u2666";
    public static final String ICON_CLUB = "\u2663";
    public static final String ICON_JOKER = "\u2648";
    public static final String ICON_A = "U+1F1E6";
    public static final String ICON_B = "U+1F1E7";
    public static final String ICON_C = "U+1F1E8";
    public static final String ICON_D = "U+1F1E9";
    public static final String ICON_E = "U+1F1EA";
    public static final String ICON_F = "U+1F1EB";
    public static final String ICON_G = "U+1F1EC";
    public static final String ICON_H = "U+1F1ED";
    public static final String ICON_I = "U+1F1EE";
    public static final String ICON_J = "U+1F1EF";
    public static final String ICON_K = "U+1F1F0";
    public static final String ICON_L = "U+1F1F1";
    public static final String ICON_M = "U+1F1F2";
    public static final String ICON_N = "U+1F1F3";
    public static final String ICON_O = "U+1F1F4";
    public static final String ICON_P = "U+1F1F5";
    public static final String ICON_Q = "U+1F1F6";
    public static final String ICON_R = "U+1F1F7";
    public static final String ICON_S = "U+1F1F8";
    public static final String ICON_T = "U+1F1F9";
    public static final String ICON_ONE = "U+31U+fe0fU+20e3";
    public static final String ICON_TWO = "U+32U+fe0fU+20e3";
    public static final String ICON_HANDS_UP = "U+270B";
    public static final String ICON_HANDS_LEFT = "U+1F448";
    public static final String ICON_HANDS_RIGHT = "U+1F449";
    public static final String TROPHY = ":trophy:";
    public static final String WAVE = ":wave:";
    public static final String QUESTION = ":question:";
    public static final String JOKER = ":black_joker:";
    public static final String ICON_1 = ":one:";
    public static final String ICON_2 = ":two:";
    public static final String SPEECH = ":speech_balloon:";
    public static final String WHITE_SQUARE = ":white_small_square:";
    public static final String FIRST_PLACE = "\uD83E\uDD47";
    public static final String SECOND_PLACE = "\uD83E\uDD48";
    public static final String THIRD_PLACE = "\uD83E\uDD49";
    public static final String ICON_X = "U+2716";
    public static final String ICON_CHECK = "U+2705";
}

