package id.ac.ui.cs.remeebot.core.state;

import id.ac.ui.cs.remeebot.core.game.gameplay.Game;
import id.ac.ui.cs.remeebot.core.game.player.Player;
import net.dv8tion.jda.api.entities.TextChannel;

import java.util.ArrayList;

public class GamePreparationState extends State {
    public GamePreparationState(Game game) {
        super(game);
    }

    @Override
    public boolean join(Player player) {
        int gameMaxPlayer = game.getMaxPlayer();
        TextChannel channel = game.getChannel();
        int sizeListPlayer = listPlayer.size();

        if (sizeListPlayer < gameMaxPlayer) {
            listPlayer.add(player);
            return true;
        } else {
            channel.sendMessage(String.format("```Pemain sudah melebihi batas maksimum (maks %s pemain)```", gameMaxPlayer)).queue();
            return false;
        }
    }

    @Override
    public boolean start() {
        int gameMinPlayer = game.getMinPlayer();
        String gameName = game.getGameName();
        TextChannel channel = game.getChannel();
        int sizeListPlayer = listPlayer.size();

        if (sizeListPlayer >= gameMinPlayer) {
            State nextState = new InGameState(game);
            game.changeState(nextState);
            channel.sendMessage(String.format("Permainan %s dimulai", gameName)).queue();
            game.deal(listPlayer);
            game.run();
            return true;
        } else {
            channel.sendMessage(String.format("```Pemain belum mencapai batas minimum (min %s pemain)```", gameMinPlayer)).queue();
            return false;
        }
    }

    @Override
    public boolean leave(Player player){
        listPlayer.remove(player);
        return true;
    }
}
