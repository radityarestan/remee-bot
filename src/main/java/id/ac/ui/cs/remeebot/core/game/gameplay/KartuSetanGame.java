package id.ac.ui.cs.remeebot.core.game.gameplay;

import id.ac.ui.cs.remeebot.core.game.card.Card;
import id.ac.ui.cs.remeebot.core.game.card.Joker;
import id.ac.ui.cs.remeebot.core.game.player.KartuSetanPlayer;
import id.ac.ui.cs.remeebot.core.game.player.Player;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;

import java.util.*;

import static id.ac.ui.cs.remeebot.core.game.ConstantEmote.*;

public class KartuSetanGame extends Game {
    private ArrayList<String> leaderboard = new ArrayList<>();
    private final List<String> reactions = new ArrayList<>(
            Arrays.asList(
                    ICON_A, ICON_B, ICON_C, ICON_D, ICON_E,
                    ICON_F, ICON_G, ICON_H, ICON_I, ICON_J,
                    ICON_K, ICON_L, ICON_M, ICON_N, ICON_O,
                    ICON_P, ICON_Q, ICON_R, ICON_S, ICON_T
            ));


    public KartuSetanGame(MessageReactionAddEvent newGameEvent) {
        super(newGameEvent);
        this.gameName = "Kartu Setan";
        maxPlayer = 8;
        minPlayer = 2;
    }

    @Override
    public void deal(ArrayList<Player> players) {
        Card kartuSetan = new Joker("Joker");
        cardRepository.addCard(kartuSetan);
        Stack<Card> cardList = cardRepository.getCardList();
        Collections.shuffle(cardList);

        while (cardList.size() > 0) {
            for (int orderOfPlayer = 0; orderOfPlayer < players.size() && cardList.size() != 0; orderOfPlayer++) {
                Player player = players.get(orderOfPlayer);
                Card newCard = cardList.pop();
                player.getCard(newCard);
            }
        }

        for (Player player : this.getPlayers()) {
            String cardsOfPlayer = player.getDist();
            sendToPrivateChannel(player, cardsOfPlayer);
        }

        for (Player player : this.getPlayers()) {
            player.setListCard(((KartuSetanPlayer) player).checkCard());
            String cardsOfPlayer = player.getDist();
            sendToPrivateChannel(player, cardsOfPlayer);
        }
        rafflePlayers();
    }

    @Override
    public void broadcast() {
        Player playerWhoGetsTurn = this.getPlayerWhoGetsTurn();
        Player playerWhoGetsDrawn = playerWhoGetsTurn.getNextPlayer();
        String cardListPlayerWhoGetsTurn = playerWhoGetsTurn.getDist();
        String cardListPlayerWhoGetsDrawn = playerWhoGetsDrawn.getDist();
        sendToPrivateChannel(playerWhoGetsTurn, cardListPlayerWhoGetsTurn);
        sendToPrivateChannel(playerWhoGetsDrawn, cardListPlayerWhoGetsDrawn);
    }

    @Override
    public void run() {
        if (this.getPlayers().size() == 0) {
            return;
        }
        Player currentPlayer = this.getPlayerWhoGetsTurn();
        String currentPlayerName = currentPlayer.getName();
        Player nextPlayer = currentPlayer.getNextPlayer();
        String nextPlayerName = nextPlayer.getName();

        TextChannel channel = this.getChannel();
        channel.sendMessage(
                String.format("%s giliran kamu bermain dengan mengambil kartu %s", currentPlayerName, nextPlayerName)).queue(
                message -> {
                    int numberOfCard = nextPlayer.getListCard().size();
                    this.setCurrentMessageId(message.getId());
                    for (int order = 0; order < numberOfCard; order++) {
                        String reaction = reactions.get(order);
                        message.addReaction(reaction).queue();

                    }
                }
        );

    }

    @Override
    public void winGame() {
        Player currentPlayer = this.getPlayerWhoGetsTurn();
        Player prevPlayer = this.getPlayerWhoGetsTurn();
        int cardSize = currentPlayer.getListCard().size();

        while (currentPlayer != prevPlayer.getNextPlayer()) {
            prevPlayer = prevPlayer.getNextPlayer();
        }

        if (cardSize == 0) {
            Player nextPlayer = currentPlayer.getNextPlayer();
            String currentPlayerName = currentPlayer.getName();

            leaderboard.add(currentPlayerName);
            prevPlayer.setNextPlayer(nextPlayer);
            this.getPlayers().remove(currentPlayer);
            this.setPlayerWhoGetsTurn(nextPlayer);

            TextChannel channel = this.getChannel();
            channel.sendMessage(
                    String.format("Selamat %s sudah menang ^_^", currentPlayerName)).queue();
        }

        int playerSize = this.getPlayers().size();
        if (playerSize == 1) {
            Player player = this.getPlayers().remove(0);
            String playerName = player.getName();
            String result = TROPHY + " LEADERBOARD\n";

            leaderboard.add(playerName);
            Guild guild = this.getChannel().getGuild();
            Game.listGame.remove(guild);
            TextChannel channel = this.getChannel();

            for (int rank = 1; rank <= leaderboard.size(); rank++) {
                String playerRank = leaderboard.get(rank - 1);
                result += String.format("%d. %s\n", rank, playerRank);
            }

            channel.sendMessage(result).queue();
        }
    }

    @Override
    public void leaveInGame(Player player) {
        Player outPlayer = player;
        Player previousPlayer = player;

        while (outPlayer != previousPlayer.getNextPlayer()) {
            previousPlayer = previousPlayer.getNextPlayer();
        }

        Player nextPlayer = outPlayer.getNextPlayer();
        previousPlayer.setNextPlayer(nextPlayer);
        players.remove(outPlayer);

        List<Card> outPlayerCards = outPlayer.getListCard();
        Player currentPlayer = nextPlayer;
        while (outPlayerCards.size() != 0) {
            currentPlayer.getCard(outPlayerCards.remove(0));
            currentPlayer = currentPlayer.getNextPlayer();
        }

        List<Player> players = this.getPlayers();
        for (Player playerToBeAnnounce : players) {
            String cardListCurrentPlayer = playerToBeAnnounce.getDist();
            sendToPrivateChannel(playerToBeAnnounce, cardListCurrentPlayer);

            List<Card> checkedCard = ((KartuSetanPlayer) playerToBeAnnounce).checkCard();
            playerToBeAnnounce.setListCard(checkedCard);
            cardListCurrentPlayer = playerToBeAnnounce.getDist();
            sendToPrivateChannel(playerToBeAnnounce, cardListCurrentPlayer);
            ((KartuSetanPlayer) playerToBeAnnounce).announceDumped();
        }

        TextChannel channel = this.getChannel();
        String messageId = this.getCurrentMessageId();
        channel.deleteMessageById(messageId).queue();
        if (this.getPlayerWhoGetsTurn() == outPlayer) this.setPlayerWhoGetsTurn(nextPlayer);
        if (this.players.size() > 1) this.run();
    }
}
