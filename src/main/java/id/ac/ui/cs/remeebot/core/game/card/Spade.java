package id.ac.ui.cs.remeebot.core.game.card;

import static id.ac.ui.cs.remeebot.core.game.ConstantEmote.ICON_SPADE;

public class Spade extends Card {
    public Spade(String name) {
        super(name);
        this.type = "Spade";
    }

    @Override
    public String toString() {
        return this.getName() + ICON_SPADE;
    }
}
