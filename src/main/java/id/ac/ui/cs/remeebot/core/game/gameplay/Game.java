package id.ac.ui.cs.remeebot.core.game.gameplay;

import id.ac.ui.cs.remeebot.core.game.player.Player;
import id.ac.ui.cs.remeebot.core.state.GamePreparationState;
import id.ac.ui.cs.remeebot.core.state.State;
import id.ac.ui.cs.remeebot.repository.CardRepository;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.Event;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public abstract class Game {
    public static Map<Guild, Game> listGame = new HashMap<>();
    protected final ArrayList<Player> players;
    private final TextChannel channel;
    protected final CardRepository cardRepository;
    private State state;
    private Player playerWhoGetsTurn;
    private Player playerWhoGetVoted;
    protected int maxPlayer;
    protected int minPlayer;
    protected String gameName;
    protected ArrayList<Player> leaderboard = new ArrayList<>();
    protected ArrayList<Player> voteList = new ArrayList<>() ;
    private String currentMessageId;


    protected Game(MessageReactionAddEvent newGameEvent) {
        this.channel = newGameEvent.getTextChannel();
        this.cardRepository = new CardRepository();
        this.players = new ArrayList<>();
        this.changeState(new GamePreparationState(this));
    }

    public State getState() {
        return state;
    }

    public void changeState(State state) {
        this.state = state;
    }

    public TextChannel getChannel() {
        return channel;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }


    public String getGameName() {
        return gameName;
    }

    public boolean joinGame(Player player) {
        return state.join(player);
    }

    public boolean startGame() {
        return state.start();
    }

    public void setPlayerWhoGetVoted(Player playerWhoGetVoted) {
        this.playerWhoGetVoted = playerWhoGetVoted;
    }

    public Player getPlayerWhoGetVoted() {
        return playerWhoGetVoted;
    }

    public ArrayList<Player> getVoteList() {
        return voteList;
    }

    public void setPlayerWhoGetsTurn(Player playerWhoGetsTurn) {
        this.playerWhoGetsTurn = playerWhoGetsTurn;
    }

    public Player getPlayerWhoGetsTurn() {
        return playerWhoGetsTurn;
    }

    public void sendToPrivateChannel(Player player, String message) {
        User userDiscord = player.getUser();
        userDiscord.openPrivateChannel().queue(
                privateChannel -> privateChannel.sendMessage(message).queue());
    }

    public void rafflePlayers() {
        ArrayList<Player> players = this.getPlayers();
        Collections.shuffle(players);

        int numberOfPlayer = this.getPlayers().size();
        for (int order = 0; order < numberOfPlayer; order++) {
            Player player = players.get(order);
            int orderOfNextPlayer = (order + 1) % numberOfPlayer;
            Player nextPlayer = players.get(orderOfNextPlayer);
            player.setNextPlayer(nextPlayer);
        }

        Player firstPlayer = players.get(0);
        setPlayerWhoGetsTurn(firstPlayer);
    }

    public int getMaxPlayer() {
        return maxPlayer;
    }

    public int getMinPlayer() {
        return minPlayer;
    }

    public void addLeaderboard(Player player) {
        this.leaderboard.add(player);
    }

    public abstract void broadcast();

    public abstract void run();

    public abstract void deal(ArrayList<Player> players);

    public abstract void winGame();

    public boolean leave(Player player){
        return this.state.leave(player);
    }
    public abstract void leaveInGame(Player player);

    public CardRepository getCardRepository() {
        return cardRepository;
    }

    public String getCurrentMessageId() {
        return currentMessageId;
    }

    public void setCurrentMessageId(String currentMessageId) {
        this.currentMessageId = currentMessageId;
    }
}
