package id.ac.ui.cs.remeebot.core.game.player;

import id.ac.ui.cs.remeebot.core.game.card.Card;
import id.ac.ui.cs.remeebot.core.game.gameplay.Game;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.requests.restaction.MessageAction;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class KartuSetanPlayer extends Player {
    HashSet<Card> dumpedCard = new HashSet<>();

    public KartuSetanPlayer(User user, Game game) {
        super(user, game);
    }

    @Override
    public Card run(String card) {
        Player playerWhoGetsDrawn = this.getNextPlayer();
        String responseInHexa = card.substring(2);
        int orderOfCard = Integer.parseInt(responseInHexa, 16) % 127462;

        Card drawnCard = playerWhoGetsDrawn.removeCard(orderOfCard);
        this.getCard(drawnCard);
        this.setListCard(this.checkCard());
        this.announceDumped();

        return drawnCard;
    }

    public List<Card> checkCard() {
        dumpedCard.removeAll(dumpedCard);
        List<Card> myListCard = this.getListCard();
        List<Card> myNewListCard = new ArrayList<>();
        for (Card card : myListCard) {
            String cardName = card.getName();
            for (Card anotherCard : myListCard) {
                String anotherCardName = anotherCard.getName();
                if (card == anotherCard)
                    continue;
                if (cardName.equals(anotherCardName) && !dumpedCard.contains(card)) {
                    if (!dumpedCard.contains(anotherCard)) {
                        dumpedCard.add(card);
                        dumpedCard.add(anotherCard);
                        break;
                    }
                }
            }
        }

        for (Card card : myListCard) {
            if (!dumpedCard.contains(card)) {
                myNewListCard.add(card);
            }
        }
        return myNewListCard;
    }

    @Override
    public int compareTo(Player o) {
        return 0;
    }

    public void announceDumped() {
        Game currentGame = this.getCurrentGame();
        TextChannel channel = currentGame.getChannel();
        String playerName = this.getName();

        if (dumpedCard.size() == 0) {
            String text = "```" + playerName + " tidak memiliki kartu yang sama" + "```";

            MessageAction messageAction = channel.sendMessage(text);
            messageAction.queue();
        } else if (dumpedCard.size() >= 0) {
            String cards = "";
            for (Card card : dumpedCard) {
                cards += card + "\n";
            }
            String text = "```" + playerName + " membuang kartu: \n" + cards + "```";
            MessageAction messageAction = channel.sendMessage(text);
            messageAction.queue();
        }
    }
}