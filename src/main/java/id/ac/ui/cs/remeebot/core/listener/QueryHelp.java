package id.ac.ui.cs.remeebot.core.listener;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.restaction.MessageAction;

import static id.ac.ui.cs.remeebot.core.game.ConstantEmote.*;


public class QueryHelp extends ListenerAdapter {
    private final String details = WAVE + " SELAMAT DATANG DI BOT REMEE " + WAVE +
            "\n\n" + QUESTION + " Apa itu Remee Bot? " + QUESTION +
            "\nRemee adalah Discord Game Bot yang terdiri dari kumpulan Card Game." +
            "\nUntuk bentuk permainannya akan dilakukan secara text-based." +
            "\n\n" + JOKER + " Permainan apa saja yang terdapat pada Remee Bot? " + JOKER +
            "\n" + ICON_1 + " Kartu Setan \n" + ICON_2 + " 41 " +
            "\n\n" + SPEECH + "Query apa saja yang dapat dijalankan dalam bot Remee? " + SPEECH +
            "\n" + WHITE_SQUARE + " **remee help**" +
            "\n" + WHITE_SQUARE + " **remee play** : Menampilkan pilihan permainan yang ada" +
            "\n" + WHITE_SQUARE + " **remee start** : Memulai permainan" +
            "\n" + WHITE_SQUARE + " **remee bye** : Mengeluarkan remee dari server" +
            "\n" + WHITE_SQUARE + " **remee stop** : Menghentikan permainan" +
            "\n" + WHITE_SQUARE + " **remee leave** : Pemain mengurungkan niatnya untuk bermain";

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        TextChannel textChannel = event.getChannel();
        User author = event.getAuthor();
        boolean isNotBot = !author.isBot();
        Message incomingText = event.getMessage();
        String rawIncomingText = incomingText.getContentRaw();
        boolean isHelp = rawIncomingText.equals("remee help");
        if (isNotBot && isHelp) {
            MessageAction toSend = textChannel.sendMessage(details);
            toSend.queue();
        }
    }
}
