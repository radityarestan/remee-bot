package id.ac.ui.cs.remeebot;

import id.ac.ui.cs.remeebot.core.listener.*;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.requests.GatewayIntent;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.security.auth.login.LoginException;

@Component
public class JDAInitializer {
    private static String token = "ODM1NzI5NjI4MzY1OTc5Njg4.YITr3g.TeTOgBd2ZdT1PuQhFjvyFCUT4DE";

    @PostConstruct
    public void jdaLogin() throws LoginException {
        JDABuilder.createDefault(
                token, GatewayIntent.GUILD_MEMBERS
        )
                .enableIntents(GatewayIntent.GUILD_MESSAGES)
                .enableIntents(GatewayIntent.GUILD_MESSAGE_REACTIONS)
                .addEventListeners(new QueryHelp())
                .addEventListeners(new QueryPlay())
                .addEventListeners(new QueryJoin())
                .addEventListeners(new QueryStart())
                .addEventListeners(new QueryLeave())
                .addEventListeners(new QueryStop())
                .addEventListeners(new QueryBye())
                .addEventListeners(new QueryKick())
                .addEventListeners(new QueryKartuSetan())
                .addEventListeners(new QueryKartu41())
                .build();
    }
}
