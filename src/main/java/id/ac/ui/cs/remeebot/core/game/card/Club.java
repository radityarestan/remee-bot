package id.ac.ui.cs.remeebot.core.game.card;

import static id.ac.ui.cs.remeebot.core.game.ConstantEmote.ICON_CLUB;

public class Club extends Card {
    public Club(String name) {
        super(name);
        this.type = "Club";
    }

    @Override
    public String toString() {
        return this.getName() + ICON_CLUB;
    }
}
