package id.ac.ui.cs.remeebot.core.listener;

import id.ac.ui.cs.remeebot.core.game.ConstantEmote;
import id.ac.ui.cs.remeebot.core.game.gameplay.Game;
import id.ac.ui.cs.remeebot.core.game.gameplay.KartuSetanGame;
import id.ac.ui.cs.remeebot.core.game.player.Player;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class QueryKartuSetan extends ListenerAdapter {
    @Override
    public void onGuildMessageReactionAdd(GuildMessageReactionAddEvent event) {
        boolean isGameExist = Game.listGame.containsKey(event.getGuild());
        boolean isPlayerExist = false;
        Game currentGame = null;
        if (isGameExist) {
            currentGame = Game.listGame.get(event.getGuild());
            isPlayerExist = currentGame.getPlayerWhoGetsTurn() != null ? true : false;
        }

        if (currentGame instanceof KartuSetanGame && isPlayerExist) {


            Player currentPlayer = currentGame.getPlayerWhoGetsTurn();
            Player nextPlayer = currentPlayer.getNextPlayer();
            User userOfCurrentPlayer = currentPlayer.getUser();
            boolean isUserWhoGetsTurn = event.getUser().equals(userOfCurrentPlayer);
            String responseCurrentUser = event.getReactionEmote().getAsCodepoints();
            boolean isYes = responseCurrentUser.equalsIgnoreCase(ConstantEmote.ICON_CHECK);
            boolean isNo = responseCurrentUser.equalsIgnoreCase(ConstantEmote.ICON_X);

            if (isUserWhoGetsTurn && !isNo && !isYes) {
                String messageId = event.getMessageId();
                currentGame.setCurrentMessageId(messageId);
                currentPlayer.run(responseCurrentUser);
                currentGame.winGame();
                currentGame.broadcast();
                currentGame.setPlayerWhoGetsTurn(nextPlayer);
                currentGame.winGame();
                currentGame.run();

                TextChannel channel = currentGame.getChannel();
                channel.deleteMessageById(messageId).queue();

            }
        }
    }
}
