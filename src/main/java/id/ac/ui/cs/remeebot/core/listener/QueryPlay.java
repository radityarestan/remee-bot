package id.ac.ui.cs.remeebot.core.listener;

import id.ac.ui.cs.remeebot.core.game.ConstantEmote;
import id.ac.ui.cs.remeebot.core.game.gameplay.Game;
import id.ac.ui.cs.remeebot.core.game.gameplay.Kartu41Game;
import id.ac.ui.cs.remeebot.core.game.gameplay.KartuSetanGame;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import static id.ac.ui.cs.remeebot.core.game.gameplay.Game.listGame;

public class QueryPlay extends ListenerAdapter {

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        User author = event.getAuthor();
        boolean isNotBot = !author.isBot();

        Guild guild = event.getGuild();
        boolean isNotActiveGame = !Game.listGame.containsKey(guild);

        Message message = event.getMessage();
        String rawMessage = message.getContentRaw();
        boolean isPlay = rawMessage.equals("remee play");
        if (isNotBot && isNotActiveGame && isPlay) {
            event.getChannel().sendMessage(event.getAuthor().getName() + " memulai game").queue();
            event.getChannel().sendMessage("Remee Game Preparation\n" +
                    event.getAuthor().getName() + " memulai permainan.\n" +
                    "Silahkan pilih games yang ingin dimainkan\n" +
                    ":one: Kartu Setan " +
                    ":two: Kartu 41").queue(pesan -> {
                pesan.addReaction("1️⃣").queue();
                pesan.addReaction("2️⃣").queue();
            });
        }
    }


    @Override
    public void onMessageReactionAdd(MessageReactionAddEvent event) {
        Game game = null;
        Member member = event.getMember();
        User memberUser = member.getUser();
        boolean isNotBot = !memberUser.isBot();
        String codepoints = event.getReactionEmote().getAsCodepoints();

        Guild guild = event.getGuild();
        boolean isNotActiveGame = !listGame.containsKey(guild);

        boolean isOneEmote = codepoints.equalsIgnoreCase(ConstantEmote.ICON_ONE);
        boolean isTwoEmote = codepoints.equalsIgnoreCase(ConstantEmote.ICON_TWO);
        if (isNotBot & isNotActiveGame) {
            if (isOneEmote) {
                game = new KartuSetanGame(event);
                Game.listGame.put(event.getGuild(), game);
            } else if (isTwoEmote) {
                game = new Kartu41Game(event);
                Game.listGame.put(event.getGuild(), game);
            }
        }

        if (game != null) {
            event.getChannel().sendMessage(game.getGameName() + " telah dipilih. Silahkan join ke permainan").queue(
                    message -> {
                        message.addReaction("✋").queue();
                    }
            );
        }

    }
}
