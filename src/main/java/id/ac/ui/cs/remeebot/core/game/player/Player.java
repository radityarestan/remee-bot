package id.ac.ui.cs.remeebot.core.game.player;

import id.ac.ui.cs.remeebot.core.game.card.Card;
import id.ac.ui.cs.remeebot.core.game.gameplay.Game;
import net.dv8tion.jda.api.entities.User;

import java.util.ArrayList;
import java.util.List;

import static id.ac.ui.cs.remeebot.core.game.ConstantEmote.*;

public abstract class Player implements Comparable<Player> {
    private User user;
    private String name;
    protected List<Card> listCard;
    private Boolean sorted;
    private Player nextPlayer;
    private Game currentGame;

    public Player(User user) {
        this.user = user;
        this.name = user.getName();
        this.sorted = false;
        this.listCard = new ArrayList<>();
    }

    public Player(User user, Game game) {
        this.user = user;
        this.name = user.getName();
        this.sorted = false;
        this.listCard = new ArrayList<>();
        this.currentGame = game;
    }

    public void getCard(Card card) {
        listCard.add(card);
    }

    public abstract Card run(String card);

    public Boolean getSorted() {
        return sorted;
    }

    public void setSorted(Boolean sorted) {
        this.sorted = sorted;
    }

    public String getDist() {
        int spade = 0;
        int heart = 0;
        int diamond = 0;
        int club = 0;

        String hasil = "This is Your Card\n";
        String Spade = "";
        String Heart = "";
        String Diamond = "";
        String Club = "";

        boolean isJokerExist = false;
        for (Card card : listCard) {
            String cardType = card.getType();
            String cardName = card.getName();
            switch (cardType) {
                case "Spade":
                    spade += 1;
                    Spade += cardName + " ";
                    break;

                case "Heart":
                    heart += 1;
                    Heart += cardName + " ";
                    break;

                case "Diamond":
                    diamond += 1;
                    Diamond += cardName + " ";
                    break;

                case "Club":
                    club += 1;
                    Club += cardName + " ";
                    break;

                case "Joker":
                    isJokerExist = true;
                    break;
            }
        }

        String defaultResult = hasil + ICON_SPADE + ":" + Spade + "\n" + ICON_HEART + ":" + Heart + "\n" +
                ICON_DIAMOND + ":" + Diamond + "\n" + ICON_CLUB + ":" + Club;
        String kartuSetanExist = "\n" + ICON_JOKER + "Kamu dapat KARTU SETAN";

        if (isJokerExist) {
            return "```" + defaultResult + kartuSetanExist + "```";
        }
        return "```" + defaultResult + "```";
    }

    public String getName() {
        return this.name;
    }

    public User getUser() {
        return this.user;
    }

    public Player getNextPlayer() {
        return nextPlayer;
    }

    public List<Card> getListCard() {
        return listCard;
    }

    public void setNextPlayer(Player nextPlayer) {
        this.nextPlayer = nextPlayer;
    }

    public Card removeCard(int orderOfCard) {
        return listCard.remove(orderOfCard);
    }

    public boolean addCard(Card card) {
        return listCard.add(card);
    }

    public void setListCard(List<Card> listCard) {
        this.listCard = listCard;
    }

    public Game getCurrentGame() {
        return this.currentGame;
    }
}
