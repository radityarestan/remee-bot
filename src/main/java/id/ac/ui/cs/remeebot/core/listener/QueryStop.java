package id.ac.ui.cs.remeebot.core.listener;

import id.ac.ui.cs.remeebot.core.game.gameplay.Game;
import id.ac.ui.cs.remeebot.core.game.player.Player;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.restaction.MessageAction;

import java.util.ArrayList;

public class QueryStop extends ListenerAdapter {
    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        User author = event.getAuthor();
        boolean isNotBot = !author.isBot();

        Guild guild = event.getGuild();
        boolean isActiveGame = Game.listGame.containsKey(guild);

        Message incomingMessage = event.getMessage();
        String rawIncomingMessage = incomingMessage.getContentRaw();
        boolean isEqualStop = rawIncomingMessage.equals("remee stop");


        if (isNotBot && isActiveGame && isEqualStop) {
            Game currentGame = Game.listGame.get(guild);
            ArrayList<Player> players =  currentGame.getPlayers();

            Member member = event.getMember();
            User user = member.getUser();
            String name = user.getName();

            boolean isPlayer = false;
            for (int i = 0; i < players.size(); i++) {
                Player player = players.get(i);
                String playerName = player.getName();
                if (playerName == null) {
                    playerName = "dummy";
                }
                boolean isSameName = playerName.equals(name);
                if (isSameName) {
                    isPlayer = true;
                    break;
                }
            }

            if (isPlayer) {
                Game.listGame.remove(guild);
                String details = "Permainan dihentikan oleh " + name;
                TextChannel channel = event.getChannel();
                MessageAction toSend = channel.sendMessage(details);
                toSend.queue();
            } else {
                String details = "Lu jangan iseng";
                TextChannel channel = event.getChannel();
                MessageAction toSend = channel.sendMessage(details);
                toSend.queue();
            }

        }
    }
}
