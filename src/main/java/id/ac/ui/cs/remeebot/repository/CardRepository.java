package id.ac.ui.cs.remeebot.repository;

import id.ac.ui.cs.remeebot.core.game.card.*;
import org.springframework.stereotype.Repository;

import java.util.Stack;

@Repository
public class CardRepository {
    private final String[] card13 = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
    private final Stack<Card> cardList;

    public CardRepository() {
        this.cardList = new Stack<>();
        this.init();
    }

    public void init() {
        for (String card : card13) {
            Card spadeCard = new Spade(card);
            Card heartCard = new Heart(card);
            Card diamondCard = new Diamond(card);
            Card clubCard = new Club(card);
            cardList.add(spadeCard);
            cardList.add(heartCard);
            cardList.add(diamondCard);
            cardList.add(clubCard);
        }
    }

    public Boolean addCard(Card card) {
        return cardList.add(card);
    }

    public Stack<Card> getCardList() {
        return cardList;
    }
}
