package id.ac.ui.cs.remeebot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RemeeBotApplication {
    public static void main(String[] args) {
        SpringApplication.run(RemeeBotApplication.class, args);
    }
}

