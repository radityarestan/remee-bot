package id.ac.ui.cs.remeebot.core.listener;

import id.ac.ui.cs.remeebot.core.game.gameplay.Game;
import id.ac.ui.cs.remeebot.core.game.player.Player;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.jetbrains.annotations.NotNull;
import static id.ac.ui.cs.remeebot.core.game.ConstantEmote.*;

import java.util.ArrayList;

public class QueryKick extends ListenerAdapter {
    @Override
    public void onGuildMessageReceived(@NotNull GuildMessageReceivedEvent event) {
        User author = event.getAuthor();
        String authorName = author.getName();

        Member member = event.getMember();
        User memberUser = member.getUser();
        boolean isNotBot = !memberUser.isBot();

        Guild guild = event.getGuild();
        boolean isActiveGame = Game.listGame.containsKey(guild);

        Message incomingMessage = event.getMessage();
        String rawIncomingMessage = incomingMessage.getContentRaw();

        String[] splitQuery = rawIncomingMessage.split(" ");
        String query = "";
        String nameKickedPlayer = "";
        if(splitQuery.length > 2){
            query = splitQuery[0] + " " + splitQuery[1];
            nameKickedPlayer = splitQuery[2];
        }

        boolean isEqualKick = query.equals("remee kick");

        if (isNotBot && isActiveGame && isEqualKick ) {
            Game game = Game.listGame.get(guild);
            ArrayList<Player> players = game.getPlayers();
            boolean authorIsInPlayer = isUserInArrayList(players,authorName);
            boolean isKickedPlayer = isUserInArrayList(players,nameKickedPlayer);
            if(authorIsInPlayer && isKickedPlayer){
                MessageChannel messageChannel = event.getChannel();
                messageChannel.sendMessage(authorName + " telah memulai vote!\n" +
                                "Terdapat 2 reaction:" +
                                "\n:white_check_mark:: Jika setuju untuk mengeluarkan player, maka tekan tombol ini." +
                                "\n:x:: Jika ingin membatalkan vote, maka tekan reaction ini." +
                                "\nApakah seluruh player setuju untuk mengeluarkan "+nameKickedPlayer+" dari permainan?").queue(message -> {
                                    message.addReaction(ICON_CHECK).queue();
                                    message.addReaction(ICON_X).queue();
                });
                Player kickedPlayer = getPlayer(players, nameKickedPlayer);
                game.setPlayerWhoGetVoted(kickedPlayer);
            }

        }
    }

    @Override
    public void onMessageReactionAdd(MessageReactionAddEvent event) {
        Member member = event.getMember();
        User memberUser = member.getUser();
        boolean isNotBot = !memberUser.isBot();

        Guild guild = event.getGuild();
        boolean isActiveGame = Game.listGame.containsKey(guild);

        String codepoints = event.getReactionEmote().getAsCodepoints();
        boolean isYes = codepoints.equalsIgnoreCase(ICON_CHECK);
        boolean isNo = codepoints.equalsIgnoreCase(ICON_X);


        if(isNotBot && isActiveGame){
            Game game = Game.listGame.get(guild);
            ArrayList<Player> players = game.getPlayers();
            ArrayList<Player> listVote = game.getVoteList();
            Player userPlayer = getAuthorPlayer(players,memberUser);
            boolean isVoted = isUserInArrayList(listVote, memberUser.getName());

            if(isYes && !isVoted){
                listVote.add(userPlayer);
                int sizeListVote = listVote.size();
                int sizeListPlayers = players.size();
                if(sizeListVote == sizeListPlayers -1){
                    TextChannel channel = game.getChannel();
                    Player kickPlayer = game.getPlayerWhoGetVoted();
                    User userKickPlayer = kickPlayer.getUser();

                    announceExit(channel, userKickPlayer, game);
                    game.leave(kickPlayer);
                    listVote.removeAll(listVote);
                    String messageId = event.getMessageId();
                    channel.deleteMessageById(messageId).queue();
                }
            }else if(isNo){
                listVote.removeAll(listVote);
                MessageChannel messageChannel = event.getChannel();
                TextChannel channel = game.getChannel();

                String messageId = event.getMessageId();
                channel.deleteMessageById(messageId).queue();
                messageChannel.sendMessage("Vote dibatalkan, silahkan lanjutkan permainan.").queue();;
            }
        }

    }

    private boolean isUserInArrayList(ArrayList<Player> players, String name) {
        for (Player player : players) {
            User playerUser = player.getUser();
            String playerName = playerUser.getName();
            boolean isSameUser = playerName.equals(name);
            if (isSameUser) {
                return true;
            }
        }
        return false;
    }

    private Player getAuthorPlayer(ArrayList<Player> players, User author) {
        for (Player player : players) {
            User playerUser = player.getUser();
            boolean isSameAuthor = playerUser.equals(author);
            if (isSameAuthor) {
                return player;
            }
        }
        return null;
    }
    private Player getPlayer(ArrayList<Player> players, String name) {
        for (Player player : players) {
            User playerUser = player.getUser();
            String playerName = playerUser.getName();
            boolean isSameUser = playerName.equals(name);
            if (isSameUser) {
                return player;
            }
        }
        return null;
    }

    private void announceExit(MessageChannel messageChannel, User author, Game game) {
        String gameName = game.getGameName();
        boolean isKartuSetan = gameName.equals("Kartu Setan");
        boolean isKartu41 = gameName.equals("Kartu 41");
        String authorName = author.getName();

        MessageAction messageAction;
        if (isKartuSetan) {
            messageAction = messageChannel.sendMessage(authorName + " exited the game");
            messageAction.queue();
        } else if (isKartu41) {
            messageAction = messageChannel.sendMessage(authorName + " exited the game");
            messageAction.queue();
        }
    }


}
