package id.ac.ui.cs.remeebot.core.state;

import id.ac.ui.cs.remeebot.core.game.gameplay.Game;
import id.ac.ui.cs.remeebot.core.game.player.Player;

import java.util.ArrayList;

public abstract class State {
    protected final Game game;
    protected ArrayList<Player> listPlayer;

    State(Game game) {
        this.game = game;
        this.listPlayer = game.getPlayers();
    }

    public abstract boolean join(Player player);

    public abstract boolean start();

    public abstract boolean leave(Player player);
}
