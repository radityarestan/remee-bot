package id.ac.ui.cs.remeebot.core.listener;

import id.ac.ui.cs.remeebot.core.game.gameplay.Game;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

import static id.ac.ui.cs.remeebot.core.game.gameplay.Game.listGame;

public class QueryStart extends ListenerAdapter {
    @Override
    public void onGuildMessageReceived(@NotNull GuildMessageReceivedEvent event) {
        Member member = event.getMember();
        User memberUser = member.getUser();
        boolean isNotBot = !memberUser.isBot();

        Guild guild = event.getGuild();
        boolean isActiveGame = listGame.containsKey(guild);

        Message incomingMessage = event.getMessage();
        String rawIncomingMessage = incomingMessage.getContentRaw();
        boolean isStart = rawIncomingMessage.equals("remee start");

        if (isStart && isNotBot && isActiveGame) {
            Game game = listGame.get(guild);
            game.startGame();
//            if (game.startGame()) {
//                TextChannel textChannel = event.getChannel();
//                MessageAction toSend = textChannel.sendMessage("Permainan dimulai");
//                toSend.queue();
//            }
        }
    }
}