package id.ac.ui.cs.remeebot.core.game.card;

public class Joker extends Card {
    public Joker(String name) {
        super(name);
        this.type = "Joker";
    }
}
